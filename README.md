# Elpa2riscV

## Introduction

We have developed this mini-app to experiment in the [RISC-V Vector Environment](https://repo.hca.bsc.es/gitlab/epi-public/risc-v-vector-simulation-environment), but it also can be executed in other machines. We reproduce one of the ELPA 2stage kernels to transform the matrix from the tridiagonal form to a banded matrix. The mini-app have 15 versions of the kernel:

0. M_REAL_REFERENCE: It is a simple version of the code written in C, compiled without any vectorization. It can be used as a reference.
1. M_REAL_SIMPLE: It is the same kernel that M_REAL_REFERENCE but allowing the compiler use autovectorization.
2. M_REAL_SIMPLE_EVI: In this kernel we merge some loops in order to let the compiler do better vectorizations
3. M_REAL_AVX2: This is one of the ELPA kernels that uses AVX2 intrinsics.
4. M_REAL_AVX512: This is one of the ELPA kernels that uses AVX512 intrinsics.
5. M_REAL_RISCV: We have created this kernel with RISC-V instrinsics using the similar pattern that we can find in the AVX2 and AVX512 kernels.
6. M_REAL_RISCV_TRANS: This is another RISC-V kernel where we try to vectorize in a transposed way. 
7. M_REAL_SIMPLE_STRIPE: This is the same kernel as M_REAL_SIMPLE, but used with striped matrices. This is the way used by ELPA to compute in CPUs in order to improve locality.
8. M_REAL_SIMPLE_F: This is the original simple kernel from ELPA. It is written in Fortran.
9. M_REAL_SIMPLE_STRIPE_F: Same kernel as M_REAL_SIMPLE_F used with striped matrices.
10. M_REAL_SIMPLE_EVI_F: Same kernel as M_REAL_SIMPLE_F trying to merge loops.
11. M_REAL_GENERIC: This is the generic ELPA kernel translated to C. This kernels contains multiple unrolls.
12. M_REAL_GENERIC_STRIPE: Same kernel as M_REAL_GENERIC, but used with striped matrices.
13. M_REAL_GENERIC_F: The original generic ELPA kernel (Fortran code).
14. M_REAL_GENERIC_STRIPE_F: Same kernel as M_REAL_GENERIC_F but used with striped matrices




## Compiling the code

### <u>riscv64</u>

You need to load LLVM environment module. Right now there are two working versions of the EPI. Version 0.7 is suitable to run in our  FPGA-based platform (BSC), but it does not support Fortran. Version 1.0 supports Fortran, but it cannot be used in the FPGA.

Run make to generate the binary with vectorial code: ***tridi2band***

```bash
> make
```

If you want to generate a trace with the **vehave** emulator in a scalar RISC-V machine you should compile setting VEHAVE=1. You also need to load the appropriate vehave module 

```bash
> module load vehave/EPI-development # or vehave/EPI-0.7-development
> make VEHAVE=1
```

This will generate a different binary (***tridi2band-vehave***). You can get important information from the different kernels. Like the vector length, the kind and the number of any executed vectorial instruction.

Set EXTRAE=1 during the compilation to generate a binary that can produce an EXTRAE trace in the FPGA platform: ***tridi2band-extrae***. You need to load the extrae module.
```bash
> module load extrae/4.0.4
> make EXTRAE=1
```
You can also generate a binary that reads hardware counters using PAPI: ***tridi2band-papi*** (less overhead than EXTRAE). Load the PAPI module and set PAPI=1 during compilation.
```bash
> module load papi/papi-like
> make PAPI=1
```
* AVX2 and AVX512 kernels are not compiled in the RISC-V architecture
* Fortran kernels are only compiled for EPI v1.0.

### <u>x86_64</u>

Makefile will use the default compiler of your environment. It will also try to detect if your CPU supports AVX2 or AVX512. It won't generate the RISC-V specific kernels. You can generate any of the followings binaries:
```bash
> make            # tridi2band
> make EXTRAE=1   # tridi2band-extrae | To get an extrae trace
> make PAPI=1     # tridi2band-papi   | To get harware counters
```
## Executing the mini-app

```bash
> ./tridi2band  [OPTION]... [FILE] [MODE]
```

* OPTION:
  *  `-nc` No check. Disable output check.
  * `-s` Short mode. Run just a few iterations. Useful for debug, shorter emulations or running in a FPGA.
* `FILE` Should be one of the checkpoints available in `ChkPoint` folder. By default, it uses `ChkPoint/cp.1024.032.256`.
* `MODE`: A number between 0 and 14. Check [introduction](#introduction) for available modes.

There are scripts available to get traces with vehave:
```bash
./run_vehave.sh ./tridi2band-vehave ...
```
or extrae:
```bash
./run_extrae.sh ./tridi2band-extrae ...
```

## ELPA2RISCV Mini-app Source Code

This mini-app is developed based on the original ELPA code and it is part of the NOMAD mini-apps suite. More details are given in the following publication:

Mas Magre I, Grima Torres R, Cela Espín JM and Gutiérrez Moreno J. The NOMAD mini-apps: A suite of kernels from ab initio electronic structure codes enabling co-design in high-performance computing. Open Reseaerch Europe 2024, 4:35 (https://doi.org/10.12688/openreseurope.16920)

### Licensing and Use
This software is heavily reliant on components from the ELPA library. As such, it adheres to the LGPL v3 license, under which the ELPA codebase is distributed. The full terms and conditions of this license are available in the COPYING folder, which is an integral part of this repository.

For further details on the LGPL v3 license, you can refer to the GNU General Public License website.

### Source Code Origin
The files contained in this distribution are inspired in several ELPA files:

* compute_hh_trafo.F90
* real_128bit_256bit_512bit_BLOCK_template.c
* simple_template.F90
* real_template.F90

The original version of these files can be found at <https://gitlab.mpcdf.mpg.de/elpa/elpa/-/tree/master/src/elpa2?ref_type=heads>

### Acknowledgements
We acknowledge the contribution of the ELPA consortium and the significance of their work in the realm of eigenvalue problem algorithms. Their efforts have been instrumental in the development of this project. 
This work has been funded and developed within the framework of the NOMAD Centre or Excellence, funded by the European Union’s Horizon 2020 research and innovation program under grant agreement No. 951786.