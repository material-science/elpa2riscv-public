#!/bin/bash

#Load Extrae
EXTRAE=extrae/4.0.6_papi-like
module is-loaded ${EXTRAE} || {
    module is-loaded papi && module unload papi
    module load ${EXTRAE}
}
export LD_LIBRARY_PATH=${PAPI_LIBS}:$LD_LIBRARY_PATH
export EXTRAE_CONFIG_FILE=xml/extrae-fpga.xml

#Execute instrumented binary
LD_PRELOAD=${EXTRAE_HOME}/lib/libseqtrace.so:${LD_PRELOAD} "$@"

#Move traces to extrae_prv_traces folder
mkdir -p extrae_prv_traces

ID=0
i=2; while [ ${i} -le $# ]; do
  [ "${!i}" -eq "${!i}" ] &> /dev/null && ID=${!i}
  i=$((i+1))
done
tracename=$( basename $1 )
finalname=$tracename-${ID}
if [[ "$LD_PRELOAD" == *"libautoHP"* ]]; then finalname=${finalname}-huge; fi
for ext in prv pcf row; do mv ${tracename}.${ext} extrae_prv_traces/fpga-${finalname}.${ext}; done
rm -rf set-0
