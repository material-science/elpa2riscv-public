ARCH=$(shell uname -m)
DEBUG=no
EXTRAE=no
VEHAVE=no
PAPI=no

$(info - ARCH=$(ARCH))
$(info - DEBUG=$(DEBUG))
$(info - EXTRAE=$(EXTRAE))
$(info - VEHAVE=$(VEHAVE))
$(info - PAPI=$(PAPI))
ifeq ($(DEBUG),$(filter $(DEBUG),1 Y y YES yes TRUE true T t))
    CFLAGS=-g -std=gnu99
    FFLAGS=-g
else
    CFLAGS=-O3 -ffast-math -std=gnu99
    FFLAGS=$(if $(findstring icc,$(CC)),-O3,-O3 -ffast-math)
endif

ifeq ($(ARCH),riscv64)
    LLVM_VERSION=$(shell which clang > /dev/null 2> /dev/null && { clang --version | grep -q "version 19" && echo "1" || echo "0.7"; } || echo "NO")
    ifeq ($(LLVM_VERSION),1)
        $(info - LLVM EPI-v1.0)
        CC=clang
        FC=flang
        FORTRAN=yes
        LLVM_TAG=
    else ifeq ($(LLVM_VERSION),0.7)
        $(info - LLVM EPI-v0.7)
        CC=clang
        FC=
        FORTRAN=no
        LLVM_TAG=-0.7
    else
        $(info - LLVM is missing. Please load one of the following modules:)
        $(info -        llvm/EPI-development)
        $(info -        llvm/EPI-0.7-development)
        $(error Aborting)
    endif    
    ifeq ($(EXTRAE),$(filter $(EXTRAE),1 Y y YES yes TRUE true T t))
        ifeq ($(EXTRAE_HOME),)
            $(error "Load EXTRAE module: module load extrae/4.0.4")
        endif
        INCLUDES+=-DEXTRAE -DEXTRAE_ALWAYS_TRACE
        LIBS=-Wl,-rpath -Wl,$(EXTRAE_HOME)/lib -L$(EXTRAE_HOME)/lib -I/$(EXTRAE_HOME)/include -lseqtrace 
        SUFFIX=-extrae
    else ifeq ($(PAPI),$(filter $(PAPI),1 Y y YES yes TRUE true T t))
        ifeq ($(PAPI_LIBS),)
            $(error "Load PAPI module: module load papi/papi-like")
        endif
        PAPI_LIB_PATH=$(shell echo $(PAPI_LIBS) | cut -c 3- )
        INCLUDES+=-DPAPI $(PAPI_INCL)
        LIBS=-Wl,-rpath -Wl,$(PAPI_LIB_PATH) $(PAPI_LIBS) -lpapi
        SUFFIX=-papi
    else ifeq ($(VEHAVE),$(filter $(VEHAVE),1 Y y YES yes TRUE true T t))
        vehave_bin=$(shell which vehave )
        ifeq ($(vehave_bin),)
            $(error Load vehave module: module load vehave/EPI$(LLVM_TAG)-development)
        endif
        VEHAVE_PATH=$(realpath $(dir $(vehave_bin))/.. )
        INCLUDES+=-DVEHAVE -I$(VEHAVE_PATH)/include/vehave
        SUFFIX=-vehave
    endif
    VFLAGS=-mepi -mllvm -combiner-store-merging=0 -Rpass=loop-vectorize -Rpass-analysis=loop-vectorize -mcpu=avispado -mllvm -vectorizer-use-vp-strided-load-store -mllvm -enable-mem-access-versioning=0 -mllvm -disable-loop-idiom-memcpy
    NO_VFLAGS=-fno-vectorize
else
    FORTRAN=yes
    ifeq ($(CC),icc)
        CFLAGS+=$(if $(findstring yes,$(shell icc 2>&1 | grep -q deprecated && echo yes )),-diag-disable=10441,)
        VFLAGS=-march=native
        NO_VFLAGS=-no-vec
        VFLAGS=-march=native
    else ifeq ($(CC),$(filter $(CC),cc gcc))
        FFLAGS+=-fallow-argument-mismatch -w
        VFLAGS=-march=native
        NO_VFLAGS=-fno-tree-vectorize
    else
        $(error Check compiler CC=$(CC))
    endif
    VFLAGS+=$(if $(findstring avx2,$(shell lscpu | grep avx2)),-D_AVX2,)
    VFLAGS+=$(if $(findstring avx512,$(shell lscpu | grep avx512)),-D_AVX512,)
    ifeq ($(PAPI),$(filter $(PAPI),1 Y y YES yes TRUE true T t))
#        KK=$(shell $(CC) -lpapi 2>&1 | grep "cannot find -lpapia" )
#        K2=$(if $(findstring "cannot find -lpapi",$(shell $(CC) -lpapi 2>&1 | grep "cannot find -lpapi" )),no,yes)
#        $(info SHELL=$(shell $(CC) -lpapi 2>&1 | grep "cannot find -lpapia" ))
        PAPI_is_missing=$(shell $(CC) -lpapi 2>&1 | grep -q "cannot find -lpapi" && echo "yes" || echo "no")
        ifeq ($(PAPI_is_missing),yes)
            $(info # PAPI library is_missing. Please load module or install it.)
            $(info #        module load papi)
            $(info #        sudo apt install libpapi-dev)
            $(error Aborting)
        endif
        INCLUDES+=-DPAPI
        LIBS=-lpapi
        SUFFIX=-papi
    endif
endif
INCLUDES+=-Isrc

SRCDIR=src
OBJDIR=obj$(SUFFIX)
BIN=tridi2band$(SUFFIX)
CFILES=$(wildcard $(SRCDIR)/*.c)
ifeq ($(FORTRAN),yes)
FFILES=$(wildcard $(SRCDIR)/*.F90)
CFLAGS+=-D_FORTRAN
endif
OBJFILES=$(addprefix $(OBJDIR)/,$(notdir $(CFILES:%.c=%.o))) \
         $(addprefix $(OBJDIR)/,$(notdir $(FFILES:%.F90=%.o)))

all: $(BIN)

$(BIN): $(OBJFILES)
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

#	llvm-objdump --mattr=+v -D $@ > $@.asm
$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $(INCLUDES) $(NO_VFLAGS) -c -o $@ $<
$(OBJDIR)/real_%.o: $(SRCDIR)/real_%.c
	$(CC) $(CFLAGS) $(INCLUDES) $(VFLAGS) -c -o $@ $<
$(OBJDIR)/real_reference.o: $(SRCDIR)/real_reference.c
	$(CC) $(CFLAGS) $(INCLUDES) $(NO_VFLAGS) -c -o $@ $<
$(OBJDIR)/%.o: $(SRCDIR)/%.F90
	$(FC) $(FFLAGS) $(INCLUDES) $(VFLAGS) -c -o $@ $<

$(OBJFILES): | $(OBJDIR)

$(OBJDIR):
	@echo PRE OBJDIR
	@-mkdir $@

clean:
	@-rm -rf $(OBJDIR) $(BIN)

cleanall:
	@-rm -rf obj* tridi2band*

$(OBJDIR)/compute_hh_trafo.o: $(SRCDIR)/utils.h
$(OBJDIR)/compute_hh_trafo.o: $(SRCDIR)/kernels.h
$(OBJDIR)/compute_hh_trafo.o: $(SRCDIR)/tracing.h
$(OBJDIR)/utils.o:            $(SRCDIR)/utils.h

$(OBJDIR)/tracing.o:            $(SRCDIR)/tracing.h
$(OBJDIR)/real_avx2.o:          $(SRCDIR)/tracing.h
$(OBJDIR)/real_avx512.o:        $(SRCDIR)/tracing.h
$(OBJDIR)/real_generic.o:       $(SRCDIR)/tracing.h
$(OBJDIR)/real_reference.o:     $(SRCDIR)/tracing.h
$(OBJDIR)/real_riscv.o:         $(SRCDIR)/tracing.h
$(OBJDIR)/real_riscv_prload.o:  $(SRCDIR)/tracing.h
$(OBJDIR)/real_riscv_trans.o:   $(SRCDIR)/tracing.h
$(OBJDIR)/real_simple.o:        $(SRCDIR)/tracing.h
$(OBJDIR)/real_simple_evi.o:    $(SRCDIR)/tracing.h
$(OBJDIR)/real_simple_trans.o:  $(SRCDIR)/tracing.h

$(OBJDIR)/real_riscv.o:         $(SRCDIR)/riscv_macros.h
$(OBJDIR)/real_riscv_prload.o:  $(SRCDIR)/riscv_macros.h
$(OBJDIR)/real_riscv_trans.o:   $(SRCDIR)/riscv_macros.h
