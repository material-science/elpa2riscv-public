#include<stdio.h>
#include<stdlib.h>
#include <stdbool.h>
#include "utils.h"
#include "tracing.h"

int nops;
extern int verbose;

#include "kernels.h"

int main( int narg, char *argv[] ) {
  int       mode;
  char     *fn;
  int       nbw, nl, lda, ncols;
  double   *a, *bcast_buffer, *a_out;
  double    time;
  unsigned long int cycles;

  mode = readArguments( narg, argv, &fn );
  readFile( mode, fn, &nbw, &nl, &ncols, &lda, &a, &bcast_buffer, &a_out );
  if (verbose) {
    printf( "\t  MODE: %d -- %s\n", mode, get_mode_str(mode) );
    printf( "\t NCOLS: %d\n", ncols );
    printf( "\t   NBW: %d\n", nbw );
    printf( "\t   LDA: %d\n", lda );
    printf( "\t    NL: %d\n", nl );
  }

  flush_cache( a, nl*(ncols+nbw) );
  flush_cache( bcast_buffer, nbw*ncols );


#if defined(PAPI) || defined(PAPI_HL)
  Init_PAPI_trace( );
#else
  time = d_gettime( );
#endif
  cycles = getcycles( );
  switch(mode) {
    case M_REAL_REFERENCE:
      real_reference( bcast_buffer, a, ncols, nbw, nl, lda );
      break;
    case M_REAL_SIMPLE:
    case M_REAL_SIMPLE_STRIPE:
      real_simple( bcast_buffer, a, ncols, nbw, nl, lda );
      break;
    case M_REAL_SIMPLE_F:
    case M_REAL_SIMPLE_STRIPE_F:
      real_simple_f( bcast_buffer, a, ncols, nbw, nl, lda );
      break;
    case M_REAL_SIMPLE_EVI_F:
      real_simple_evi_f( bcast_buffer, a, ncols, nbw, nl, lda );
      break;
    case M_REAL_GENERIC:
    case M_REAL_GENERIC_STRIPE:
      real_generic( bcast_buffer, a, ncols, nbw, nl, lda );
      break;
    case M_REAL_GENERIC_F:
    case M_REAL_GENERIC_STRIPE_F:
      real_generic_f( bcast_buffer, a, ncols, nbw, nl, lda );
      break;
    case M_REAL_SIMPLE_EVI:
      real_simple_evi( bcast_buffer, a, ncols, nbw, nl );
      break;
    case M_REAL_SIMPLE_TRANS:
      real_simple_trans( bcast_buffer, a, ncols, nbw, nl );
      break;
    case M_REAL_AVX2:
      real_avx2( bcast_buffer, a, ncols, nbw, nl );
      break;
    case M_REAL_AVX512:
      real_avx512( bcast_buffer, a, ncols, nbw, nl );
      break;
    case M_REAL_RISCV:
      real_riscv( bcast_buffer, a, ncols, nbw, nl );
      break;
    case M_REAL_RISCV_TRANS:
      real_riscv_trans( bcast_buffer, a, ncols, nbw, nl );
      break;
    default:
      printf( "ERROR: Bad mode=%d\n", mode ); exit(0);
  }
  cycles = getcycles( ) - cycles;
#if defined(PAPI) || defined(PAPI_HL)
  Finish_PAPI_trace( );
  printf( "%20s: %lu cycles\n", "CYCLES", cycles );
#else
  time = d_gettime( )-time;
  if (verbose) {
    printf( "\t  TIME: %10.4E\n", nops==0? -0.0 : time );
    printf( "\t  NOPS: %d\n", nops );
    printf( "\tCYCLES: %lu cycles\n", cycles );
    printf( "\t FLOPS: %lf GFlops\n", (nops/time)/(1024*1024*1024) );
  } else  printf( "%2d %-24s: %7.4lf GFlops -- %lu cycles\n", mode, get_mode_str(mode), (nops/time)/(1024*1024*1024), cycles );
#endif


  if (nops!=0) checkDiff( a, a_out, nl*(ncols+nbw) );

  free(a);
  free(bcast_buffer);
  free(a_out);
  return 0;
}
