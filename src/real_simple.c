#include <stdlib.h>
#include <stdio.h>
#include "tracing.h"

extern int nops;

static void hh_trafo_simple(double* q, double* hh[2], int nb,
            int nq, double *x, double *y) {
  const int ldq=nq;
  const int ldh=nb;
  int i, j;

  double s = hh[1][1];
  for( j=2; j<nb; j++ ) s = s + hh[1][j]*hh[0][j-1];

  for( i=0; i<nq; i++ ) x[i] = q[i+ldq];
  for( i=0; i<nq; i++ ) y[i] = q[i] + q[i+ldq]*hh[1][1];
  for( j = 2; j < nb; j++ ) {
    double h1 = hh[0][j-1];
    double h2 = hh[1][j];
    for( i=0; i<nq; i++ ) x[i] = x[i] + q[i+j*ldq]*h1;
    for( i=0; i<nq; i++ ) y[i] = y[i] + q[i+j*ldq]*h2;
  }
  for( i=0; i<nq; i++ ) x[i] = x[i] + q[i+nb*ldq] * hh[0][nb-1];

  double tau1 = hh[0][0];
  double tau2 = hh[1][0];
  double h1 = -tau1;
  for( i = 0; i < nq; i++ ) x[i] = x[i] * h1;
  h1 = -tau2;
  double h2 = -tau2 * s;

  for( i=0; i<nq; i++ ) y[i] = y[i]*h1 + x[i]*h2;
  for( i=0; i<nq; i++ ) q[i] = q[i] + y[i];
  for( i=0; i<nq; i++ ) q[i+ldq] = q[i+ldq] + x[i] + y[i]*hh[1][1];
  for( j=2; j<nb; j++ ) {
    double h1 = hh[0][j-1];
    double h2 = hh[1][j];
    for( i=0; i<nq; i++ ) q[i+j*ldq] = q[i+j*ldq] + x[i]*h1 + y[i]*h2;
  }
  for( i=0; i<nq; i++ ) q[i+nb*ldq] = q[i+nb*ldq] + x[i]*hh[0][nb-1];
}

void real_simple( double *bcast_buffer, double* q, int ncols, int nbw, int nq, int ldq ) {
  int err;
  double *w[2];
  double *x, *y;

  err=posix_memalign( (void **)&x, 64,  nq*sizeof(double) );
  err=posix_memalign( (void **)&y, 64,  nq*sizeof(double) );

  initialize_tracing();
  enable_tracing();
  trace_event(1000,1);

  nops=0;
  int off=0;
  for (int i=0; i< nq; i+=ldq) {
    int lnq = ldq< nq-i ? ldq : nq-i;
    for (int j=ncols-1; j>=0; j-=2) {
      w[0] = &bcast_buffer[j*nbw];
      w[1] = &bcast_buffer[(j-1)*nbw];
      hh_trafo_simple( &q[off+(j-1)*lnq], w, nbw, lnq, x, y );
    }
    off += lnq*(ncols+nbw);
  }
  nops = (nq*nbw*8-2*nq+4)*ncols/2 + 2*(nbw-2);
  trace_event(1000,0);

  free(x);
  free(y);
}
