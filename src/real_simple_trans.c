#include <stdlib.h>
#include "tracing.h"

extern int nops;

static void hh_trafo_simple_trans(double* q, double* hh[2], int nb, int nq) {
  const int ldq=nq;
  const int ldh=nb;
  double s, h1, h2, tau1, tau2;
  double x;
  double y;
  int i, j;

  s = hh[1][1];
  for( i = 2; i < nb; i++ )
    s = s + hh[1][i]*hh[0][i-1];

  for( i = 0; i < nq; i++ ) {
    x = q[i+ldq];
    y = q[i] + q[i+ldq]*hh[1][1];

    for( j = 2; j < nb; j++ ) {
      h1 = hh[0][j-1];
      h2 = hh[1][j];
      x += q[i+j*ldq]*h1;
      y += q[i+j*ldq]*h2;
    }
    x = x + q[i+nb*ldq] * hh[0][nb-1];

    tau1 = hh[0][0];
    tau2 = hh[1][0];
    h1 = -tau1;

    x = x * h1;
    h1 = -tau2;
    h2 = -tau2 * s;

    y = y*h1 + x*h2;

    q[i] = q[i] + y;
    q[i+ldq] = q[i+ldq] + x + y*hh[1][1];

    for( j = 2; j < nb; j++ ) {
      h1 = hh[0][j-1];
      h2 = hh[1][j];
      q[i+j*ldq] += + x*h1 + y*h2;
    }
    q[i+nb*ldq] += x*hh[0][nb-1];
  }
}

void real_simple_trans( double *bcast_buffer, double* q, int ncols, int nbw, int nq ) {
  double *w[2];
  initialize_tracing();
  enable_tracing();
  trace_event(1000,1);
  for (int j=ncols-1; j>=0; j-=2) {
    w[0] = &bcast_buffer[j*nbw];
    w[1] = &bcast_buffer[(j-1)*nbw];
    hh_trafo_simple_trans( &q[(j-1)*nq], w, nbw, nq );
  }
  nops = nq*(nbw*8+2)*ncols/2 + 2*(nbw-2);
  trace_event(1000,0);
}
