#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<stdbool.h>
#include<ctype.h>
#include<math.h>
#include<errno.h>
#include<time.h>
#include"utils.h"

int verbose=0;
int checkdiff=1;
int short_run=0;

const char *mode_str[] = MODES_STR( );

#define _RSET "\e[m"
#define _BOLD "\e[1m"
#define _UNDR "\e[4m"
#define _ERRC "\033[0;31m"
#define _OKC  "\033[0;32m"

#define READ_ARRAY( ARR, D1, D2 ) { \
      int siz = D1*D2; \
      if (posix_memalign( (void **)ARR, 64,  siz*sizeof(double) )!=0) { \
      printf( "posix_memalign error @readFile\n" ); exit(0); }; \
      for (int i=0; i< siz; ) { \
        int n = fscanf( fp, "%lf %lf %lf\n", *ARR+i,*ARR+i+1, *ARR+i+2 ); \
        i+=n; \
        if (n==0 ) exit(0); \
      } \
    }

const char *get_mode_str( int mode ) { return mode_str[mode]; }

void readFile( int mode, char *fn, int *nbw, int *nl, int *ncols, int *lda,
               double **a, double **bcast_buffer, double **a_out ) {
  FILE *fp;
  size_t len = 0;
  ssize_t read;
  char tag[32];
  char *line = (char *)malloc( 256 );
  int d1, d2;
  double v1, v2, v3;

  fp = fopen( fn, "r" );
  if (fp==NULL) { printf( "ERROR: Cannot open file %s\n", fn ); exit(1); }
  while(1) {
    read = getline( &line, &len, fp );
    if (read<=0) break;
    sscanf( line, "%s %d %d", tag, &d1, &d2 );
    /* printf( "TAG=%s %d\n", tag, d1 ); */
    if(strcmp(tag,"nbw")==0) *nbw = d1;
    else if(strcmp(tag,"nl")==0) { *nl = d1;  *lda = d1; }
    else if(strcmp(tag,"ncols")==0) { *ncols = d1; }
    else if(strcmp(tag,"a")==0) { READ_ARRAY( a, d1, d2 ); }
    else if(strcmp(tag,"bcast_buffer")==0) { READ_ARRAY( bcast_buffer, d1, d2 ); }
    else if(strcmp(tag,"a_out")==0) { READ_ARRAY( a_out, d1, d2 ); }
    else break;
  }
  if (( mode==M_REAL_SIMPLE_STRIPE || mode==M_REAL_SIMPLE_STRIPE_F ||
        mode==M_REAL_GENERIC_STRIPE || mode==M_REAL_GENERIC_STRIPE_F ||
        mode==M_REAL_AVX2 || mode==M_REAL_AVX512 ) &&
       *nl>DEFAULT_STRIPE_WIDTH) {
    int ld = *nl;
    int n = *ncols + *nbw;
    double *A_dst;
    if (posix_memalign( (void **)&A_dst, 64,  n*ld*sizeof(double) )!=0) {
      printf( "posix_memalign error @readFile\n" );
      exit(0);
    }
    for (int it=0; it< 2; it++) {
      double *A_src = it==0 ? *a : *a_out;
      int i_dst=0;
      *lda = DEFAULT_STRIPE_WIDTH;

      for (int i=0; i< ld; i+=DEFAULT_STRIPE_WIDTH) {
        int imax= i+DEFAULT_STRIPE_WIDTH< ld ? i+DEFAULT_STRIPE_WIDTH : ld;
        for (int j=0; j< n; j++)
          for (int i2=i; i2< imax; i2++, i_dst++)
            A_dst[i_dst] = A_src[i2+j*ld];
      }
      if (it==0) {
        *a = A_dst;
        A_dst = A_src;
      } else {
        *a_out = A_dst;
        free( A_src );
      }
    }
  }
  if (short_run) *ncols=*ncols>NCOLS_SHORT_MODE? NCOLS_SHORT_MODE : *ncols;
}

bool IsDigit( char *ptr, int *v ) {
  *v=atoi(ptr);
  return *v!=0 ? 1 : strcmp(ptr,"0")==0;
}

void printHelp( ) {
  printf( "%sTRIDI2BAND(3)%s\n", _BOLD, _RSET );
  printf( "\n" );
  printf( "%sNAME%s\n", _BOLD, _RSET );
  printf( "\ttridi2band - ELPA kernel to transformn a tridiagonal matrix to a band matrix.\n" );
  printf( "\n" );
  printf( "%sSYNOPSIS%s\n", _BOLD, _RSET );
  printf( "\ttridi2band  [%sOPTION%s]... [%sFILE%s] [%sMODE%s]\n", _UNDR, _RSET, _UNDR, _RSET, _UNDR, _RSET );
  printf( "\n" );
  printf( "%sDESCRIPTION%s\n", _BOLD, _RSET );
  printf( "\tApply tridiagonal to band ELPA kernel to the matrix\n"
          "\tinside the FILE using one of the available MODE(s).\n" );
  printf( "\n" );
  printf( "\tWith no FILE, use the default one: %s.\n", DEFAULT_FN );
  printf( "\tWith no MODE, use the default one: %d. Available %sMODE(S)%s:\n", DEFAULT_MODE, _BOLD, _RSET );
  printf( "\n" );
  printf( "\t%s-h, --help%s\n", _BOLD, _RSET );
  printf( "\t\tdisplay this help and exit.\n" );
  printf( "\n" );
  printf( "\t%s-v, --verbose%s\n", _BOLD, _RSET );
  printf( "\t\tShows extra information.\n" );
  printf( "\n" );
  printf( "\t%s-nc, --no-check%s\n", _BOLD, _RSET );
  printf( "\t\tDisable output check.\n" );
  printf( "\n" );
  printf( "\t%s-s, --short%s\n", _BOLD, _RSET );
  printf( "\t\tRun just a few iterations (Useful for debug). Short mode implies no-check.\n" );
  printf( "\n" );

  printf( "%sENVIRONMENT VARIABLES%s\n", _BOLD, _RSET );
  printf( "\t%sMAX_ERROR%s Maximum allowed error for every element of the output matrix.\n", _BOLD, _RSET );
  printf( "\n" );

  printf( "%sAVAILABLE MODES%s\n", _BOLD, _RSET );
  for (int i=0; i< MAX_MODES; i++)
    printf( "\t%s%d%s\t%s\n", _BOLD, i, _RSET, mode_str[i] );
  printf( "\n" );

  exit(0);
}

int readArguments( int narg, char *argv[], char **fn ) {
  int m;
  int mode = DEFAULT_MODE;
  int n_modes = 0;
  *fn = NULL;
  for (int i=1; i< narg; i++) {
    if (!strcmp( argv[i], "-h") || !strcmp( argv[i], "--help")) printHelp( );
    else if (!strcmp( argv[i], "-v") || !strcmp( argv[i], "--verbose"))  verbose = 1;
    else if (!strcmp( argv[i], "-nc") || !strcmp( argv[i], "--no-check"))  checkdiff = 0;
    else if (!strcmp( argv[i], "-s") || !strcmp( argv[i], "--short")) {
      checkdiff = 0;
      short_run = 1;
    } else if (IsDigit(argv[i],&m)) {
      n_modes++;
      if (n_modes>1) { fprintf( stderr, "ERROR: Too much MODEs\n" ); exit(1); }
      if (m>=0 && m<MAX_MODES) mode = m;
      else { fprintf( stderr, "ERROR: Unknown mode [0..%d]\n", MAX_MODES-1 ); exit(1); }
    } else if (argv[i][0]=='-') {
      fprintf( stderr, "ERROR: Unknown option: %s\n", argv[i] );
      exit(1);
    } else {
      if (*fn!=NULL) { fprintf( stderr, "ERROR: Too much FILEs\n" ); exit(1); }
      *fn = argv[i];
    }
  }
  if (*fn == NULL) *fn = DEFAULT_FN;
  return mode;
}

void checkDiff( double *a, double *b, int n ) {
  int k=0;
  int nerr=0, ndiff=0;
  double diff;
  double max_err = DEFAULT_MAX_ERROR;
  if (checkdiff) {
    char *err = getenv( "MAX_ERROR" );
    if (err!=NULL) {
      char *eptr;
      errno = 0;
      double v = strtod( err, &eptr );
      if (errno==0 && v>0.0 && !strlen(eptr)) max_err = v;
    }

    for (int i=0; i< n; i++) {
      diff = fabs( a[i]-b[i] );
      if (diff!=0.0) ndiff++;
        if (diff> max_err) {
          printf( "A[%d] = %24.12E %24.12E -- %24.12E\n", i, a[i], b[i], diff );
          nerr++;
        }
      }
    printf( "\t MATRIX %s%s%s\n", _BOLD, nerr? _ERRC " ERROR" : _OKC " OK", _RSET );
    if (verbose) printf( "Number of errors: %d  -- ndiff: %d/%d\n", nerr, ndiff, n );
  }
}

double d_gettime( ) {
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  return (double)t.tv_sec + 1.0e-9*t.tv_nsec;
}

unsigned long int getcycles( ) {
# if defined(__riscv)
    unsigned long int counter;
    asm volatile("csrr %0, cycle" : "=r"(counter)  );
    return counter;
# elif defined(__x86_64)
    unsigned long a, d;
   __asm__ volatile("rdtsc" : "=a" (a), "=d" (d));
   return (a | (d << 32));
/*
  https://github.com/jdmccalpin/low-overhead-timers/blob/master/low_overhead_timers.c
      unsigned long a, d, c;
      __asm__ volatile("rdtscp" : "=a" (a), "=d" (d), "=c" (c));
      return (a | (d << 32));

      
      unsigned int lo,hi;
      __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
      return ((unsigned long int)hi << 32) | lo;
*/
# else
#   warning "TO DO getcycles"
    counter=0;
#endif
}

#define CACHE_LINE_SIZE 64
void flush_cache( double *a, int n ) {
#ifdef __x86_64
  for ( double *p=a; p<a+n; p+=CACHE_LINE_SIZE/8 )
    asm volatile( "clflush %0" : "+m" (*(volatile double *)p));
  /* asm volatile( "clflush (%0)" :: "r"(p)); */
  /* asm volatile("wbinvd" ::: "memory"); */
#endif

  double tmp = 0.0;
  char *str = getenv( "FLUSH_CACHE" );
  if (str!=NULL) {
    const int SIZE=32*1024*1024;
    double *arr = (double *)malloc(SIZE*sizeof(double));
    for (int i=0; i< SIZE; i++) arr[i] = i%100*((i+1)/100);
    for (int i=0; i< SIZE; i++) tmp += arr[i];
    free(arr);
  }
}

