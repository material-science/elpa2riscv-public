#ifndef __KERNELS__
#define __KERNELS__

void real_reference( double *bcast_buffer, double* q, int ncols, int nbw, int nq, int ldq );
void real_simple( double *bcast_buffer, double* q, int ncols, int nbw, int nq, int ldq );
void real_generic( double *bcast_buffer, double* q, int ncols, int nbw, int nq, int ldq );

void real_simple_evi( double *bcast_buffer, double* q, int ncols, int nbw, int nq );
void real_simple_trans( double *bcast_buffer, double* q, int ncols, int nbw, int nq );

void real_simple_f_( double *bcast_buffer, double* q, int *ncols, int *nbw, int *nq, int *ldq );
void real_simple_f( double *bcast_buffer, double* q, int ncols, int nbw, int nq, int ldq ) {
#ifdef _FORTRAN
  real_simple_f_( bcast_buffer, q, &ncols, &nbw, &nq, &ldq );
  nops = (nq*nbw*8-2*nq+4)*ncols/2 + 2*(nbw-2);
#else
  nops=0;
#endif
}
void real_simple_evi_f_( double *bcast_buffer, double* q, int *ncols, int *nbw, int *nq, int *ldq );
void real_simple_evi_f( double *bcast_buffer, double* q, int ncols, int nbw, int nq, int ldq ) {
#ifdef _FORTRAN
  real_simple_evi_f_( bcast_buffer, q, &ncols, &nbw, &nq, &ldq );
  nops = (nq*nbw*8-2*nq+4)*ncols/2 + 2*(nbw-2);
#else
  nops=0;
#endif
}

void real_generic_f_( double *bcast_buffer, double* q, int *ncols, int *nbw, int *nq, int *ldq );

void real_generic_f( double *bcast_buffer, double* q, int ncols, int nbw, int nq, int ldq ) {
#ifdef _FORTRAN
  real_generic_f_( bcast_buffer, q, &ncols, &nbw, &nq, &ldq );
  nops = (nq*nbw*8-2*nq+4)*ncols/2 + 2*(nbw-2);
#else
  nops=0;
#endif
}

void real_avx2( double *bcast_buffer, double* q, int ncols, int nbw, int nq );
void real_avx512( double *bcast_buffer, double* q, int ncols, int nbw, int nq );
void real_riscv( double *bcast_buffer, double* q, int ncols, int nbw, int nq );
void real_riscv_trans( double *bcast_buffer, double* q, int ncols, int nbw, int nq );

#endif
