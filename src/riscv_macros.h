#ifndef __RISCV_MACROS__
#define __RISCV_MACROS__

#if __riscv_vector_version == 800
#define DOUBLE_REG __epi_1xf64

#define SET_VECTOR_LENGTH(X)  	        __builtin_epi_vsetvl( X, __epi_e64, __epi_m1 )
#define GET_MAX_VECTOR_LENGTH( )        __builtin_epi_vsetvlmax( __epi_e64, __epi_m1 )

#define VECTOR_LOAD(x,vl)               __builtin_epi_vload_1xf64(x,vl)
#define VECTOR_STORE(x,Y,vl)            __builtin_epi_vstore_1xf64(x,Y,vl)
#define VECTOR_LOAD_STRIDED(x,st,vl)    __builtin_epi_vload_strided_1xf64(x,st,vl)
#define VECTOR_STORE_STRIDED(x,Y,st,vl) __builtin_epi_vstore_nt_strided_1xf64(x,Y,st,vl)

#define VECTOR_SET(X,vl)                __builtin_epi_vfmv_v_f_1xf64(X,vl)
#define VECTOR_GET(X)                   __builtin_epi_vfmv_f_s_1xf64(X)
#define VECTOR_ADDITION(X,Y,vl)         __builtin_epi_vfadd_1xf64(X,Y,vl)

#define MULTIPLY_ACC(X,Y,Z,vl)          __builtin_epi_vfmacc_1xf64(X,Y,Z,vl)
#define VECTOR_MULTIPLY(X,Y,vl)         __builtin_epi_vfmul_1xf64(X,Y,vl)
#define XOR_SIGN(X,Y,vl)                __builtin_epi_vfsgnjx_1xf64(X,Y,vl)

#define REDUCE_SUM(X,Y,vl)              __builtin_epi_vfredsum_1xf64(X,Y,vl)
#define GET_FIRST(X)                    __builtin_epi_vfmv_f_s_1xf64(X)

#elif __riscv_vector_version == 700

#define DOUBLE_REG __epi_1xf64

#define SET_VECTOR_LENGTH(X)  	        __builtin_epi_vsetvl( X, __epi_e64, __epi_m1 )
#define GET_MAX_VECTOR_LENGTH( )        __builtin_epi_vsetvlmax( __epi_e64, __epi_m1 )

#define VECTOR_LOAD(x,vl)               __builtin_epi_vload_1xf64(x,vl)
#define VECTOR_STORE(x,Y,vl)            __builtin_epi_vstore_1xf64(x,Y,vl)
#define VECTOR_LOAD_STRIDED(x,st,vl)    __builtin_epi_vload_strided_1xf64(x,st,vl)
#define VECTOR_STORE_STRIDED(x,Y,st,vl) __builtin_epi_vstore_nt_strided_1xf64(x,Y,st,vl)

#define VECTOR_SET(X,vl)                __builtin_epi_vbroadcast_1xf64(X,vl)
#define VECTOR_GET(X)                   __builtin_epi_vgetfirst_1xf64(X)
#define VECTOR_ADDITION(X,Y,vl)         __builtin_epi_vfadd_1xf64(X,Y,vl)

#define MULTIPLY_ACC(X,Y,Z,vl)          __builtin_epi_vfmacc_1xf64(X,Y,Z,vl)
#define VECTOR_MULTIPLY(X,Y,vl)         __builtin_epi_vfmul_1xf64(X,Y,vl)
#define XOR_SIGN(X,Y,vl)                __builtin_epi_vfsgnjx_1xf64(X,Y,vl)

#define REDUCE_SUM(X,Y,vl)              __builtin_epi_vfredsum_1xf64(X,Y,vl)
#define GET_FIRST(X)                    __builtin_epi_vfmv_f_s_1xf64(X)


#else
#error "Check this"
#endif

#endif
