#include <stdlib.h>
#include <stdio.h>
#include "tracing.h"

extern int nops;

static void hh_trafo_simple_evi(double* q, double* hh[2], int nb, int nq) {
  const int ldq=nq;
  const int ldh=nb;
  double s, h1, h2, tau1, tau2;
  double* x = (double *)malloc(nq*sizeof(double));
  double* y = (double *)malloc(nq*sizeof(double));
  int i, j;

  s = hh[1][1];
  for( i = 2; i < nb; i++ )
    s = s + hh[1][i]*hh[0][i-1];

  for( i = 0; i < nq; i++ ) {
    x[i] = q[i+ldq];
    y[i] = q[i] + q[i+ldq]*hh[1][1];
  }

  for( j = 2; j < nb; j++ ) {
    h1 = hh[0][j-1];
    h2 = hh[1][j];
    for( int i = 0; i < nq; i++ ) {
      x[i] = x[i] + q[i+j*ldq]*h1;
      y[i] = y[i] + q[i+j*ldq]*h2;
    }
  }

  for( i=0; i < nq; i++ )
    x[i] = x[i] + q[i+nb*ldq] * hh[0][nb-1];

  tau1 = hh[0][0];
  tau2 = hh[1][0];
  h1 = -tau1;
  for( i = 0; i < nq; i++ )
    x[i] = x[i] * h1;
  h1 = -tau2;
  h2 = -tau2 * s;

  for( i = 0; i < nq; i++ ) {
    y[i] = y[i]*h1 + x[i]*h2;
    q[i] = q[i] + y[i];
    q[i+ldq] = q[i+ldq] + x[i] + y[i]*hh[1][1];
  }

  for( j = 2; j < nb; j++ ) {
    h1 = hh[0][j-1];
    h2 = hh[1][j];
    for( int i=0; i < nq; i++ ) {
      q[i+j*ldq] = q[i+j*ldq] + x[i]*h1 + y[i]*h2;
    }
  }

  for( i=0; i < nq; i++ )
    q[i+nb*ldq] = q[i+nb*ldq] + x[i]*hh[0][nb-1];

  free(x);
  free(y);
}

void real_simple_evi( double *bcast_buffer, double* q, int ncols, int nbw, int nq ) {
  double *w[2];
  initialize_tracing();
  enable_tracing();
  trace_event(1000,1);
  nops=0;
  for (int j=ncols-1; j>=0; j-=2) {
    w[0] = &bcast_buffer[j*nbw];
    w[1] = &bcast_buffer[(j-1)*nbw];
    hh_trafo_simple_evi( &q[(j-1)*nq], w, nbw, nq );
  }
  nops = (nq*nbw*8-2*nq+4)*ncols/2 + 2*(nbw-2);
  trace_event(1000,0);
}
