#ifndef __MY_UTILS__
#define __MY_UTILS__

/* #define DEFAULT_FN      "ChkPoint/cp.0032.032.032" */
#define DEFAULT_FN      "ChkPoint/cp.1024.032.256"

#define DEFAULT_STRIPE_WIDTH 48
#define M_REAL_REFERENCE              0
#define M_REAL_SIMPLE                 1
#define M_REAL_SIMPLE_EVI             2
#define M_REAL_AVX2                   3
#define M_REAL_AVX512                 4
#define M_REAL_RISCV                  5
#define M_REAL_RISCV_TRANS            6
#define M_REAL_SIMPLE_STRIPE          7
#define M_REAL_SIMPLE_F               8
#define M_REAL_SIMPLE_STRIPE_F        9
#define M_REAL_SIMPLE_EVI_F           10
#define M_REAL_GENERIC                11
#define M_REAL_GENERIC_STRIPE         12
#define M_REAL_GENERIC_F              13
#define M_REAL_GENERIC_STRIPE_F       14

#define MAX_MODES                     15
#define M_REAL_SIMPLE_TRANS           15

#define DEFAULT_MODE 	      M_REAL_SIMPLE

#define MODES_STR( ) { \
        "REFERENCE", \
        "SIMPLE", \
        "SIMPLE EVIDENT", \
        "AVX2", \
        "AVX512", \
        "RISCV", \
        "RISCV TRANSPOSE", \
        "SIMPLE STRIPE", \
        "SIMPLE FORTAN", \
        "SIMPLE STRIPE FORTAN", \
        "SIMPLE EVIDENT FORTAN", \
        "GENERIC", \
        "GENERIC STRIPE", \
        "GENERIC FORTAN", \
        "GENERIC STRIPE FORTAN" \
         }
/*        "SIMPLE TRANSPOSE" */

#define DEFAULT_MAX_ERROR 1E-14
#define NCOLS_SHORT_MODE  64

  int readArguments( int narg, char *argv[], char **fn );
  void readFile( int mode, char *fn, int *nbw, int *nl, int *ncols, int *lda,
                 double **a, double **bcast_buffer, double **a_out );
  void checkDiff( double *a, double *b, int n );

  const char *get_mode_str( int mode );

  double d_gettime( );
  unsigned long int getcycles( );

  void flush_cache( double *a, int n );
#endif
