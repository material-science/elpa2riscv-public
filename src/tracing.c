#include <stdbool.h>
#include "tracing.h"

#define N_EVENTS 16
  static int         papi_eventset = -1;
  static bool        event_on[N_EVENTS];
  static const char *str_event[N_EVENTS] = {
      "PAPI_TOT_CYC",
      "PAPI_TOT_INS",
      "PAPI_VEC_INS",
      "PAPI_VEC_DP",
      "FP_ARITH_INST_RETIRED:128B_PACKED_DOUBLE",
      "FP_ARITH_INST_RETIRED:256B_PACKED_DOUBLE",
      "FP_ARITH_INST_RETIRED:512B_PACKED_DOUBLE",
      "FP_ARITH_INST_RETIRED:SCALAR_DOUBLE",
      "VPU_ACTIVE",
      "VPU_ISSUED_INST",
      "CPU_CLK_UNHALTED",
      "CPU_CLK_UNHALTED:DISTRIBUTED",
      "CPU_CLK_UNHALTED:REF_DISTRIBUTED",
      "CPU_CLK_UNHALTED:REF_XCLK",
      "CPU_CLK_UNHALTED:REF_TSC",
      "CPU_CLK_UNHALTED::THREAD_P",
  };
#if 0
#if defined(__x86_64__)
#define VECTORIAL_ACTIVE       "FP_ARITH:512B_PACKED_DOUBLE"
#define VECTORIAL_INSTRUCTIONS "PAPI_VEC_DP"
#else
#define VECTORIAL_ACTIVE       "VPU_ACTIVE"
#define VECTORIAL_INSTRUCTIONS "VPU_ISSUED_INST"
#endif
#endif

/*      "PAPI_VEC_SP" */
/*      "FP_ARITH:128B_PACKED_DOUBLE" */

#define PAPI_check_error(func) {\
  long int code = func; \
  if (code != PAPI_OK) {\
    fprintf(stderr, "PAPI error %ld at %s : %s : %d\n", code, __FILE__, __func__, __LINE__);\
    if (code==PAPI_EINVAL) fprintf(stderr, "        Invalid argument\n" ); \
    else if (code==PAPI_ECMP) fprintf(stderr, "        Not supported by component\n" ); \
    else if (code==PAPI_ECMP_DISABLED) fprintf(stderr, "        Component containing event is disabled\n" ); \
    else if (code==PAPI_ECNFLCT) fprintf(stderr, "       Event exists, but cannot be counted due to counter resource limitations\n" ); \
    else if (code==PAPI_ENOTPRESET) fprintf(stderr, "       Event in argument is not a valid preset\n" ); \
    exit(-1);\
    }\
  }


void Init_PAPI_trace( ) {
#if defined(PAPI_HL)
  PAPI_hl_region_begin("computation");
#elif defined(PAPI)
  int   ret;
  int   papi_codes[N_EVENTS];

  PAPI_library_init(PAPI_VER_CURRENT);
  PAPI_check_error( PAPI_create_eventset( &papi_eventset ) );

  for (int i=0; i<N_EVENTS; i++) {
    event_on[i] = false;
    if (PAPI_event_name_to_code( str_event[i], &papi_codes[i] )!=PAPI_OK) continue;
    if (PAPI_add_event( papi_eventset, papi_codes[i] )!=PAPI_OK) continue;
    event_on[i] = true;
  }
#if 1
  PAPI_check_error( PAPI_start(papi_eventset) );
  PAPI_check_error( PAPI_reset(papi_eventset) );
#else
  ret = PAPI_start(papi_eventset);
  ret = PAPI_reset(papi_eventset);
#endif


#endif
}

void Finish_PAPI_trace( ) {
#if defined(PAPI_HL)
  PAPI_hl_region_end("computation");
#elif defined(PAPI)
  long long papi_values[N_EVENTS];
  PAPI_check_error( PAPI_read(papi_eventset, papi_values) );
  for (int i=0, j=0; i<N_EVENTS; i++) {
    printf( "%20s: %lld\n", str_event[i], event_on[i] ? papi_values[j] : -1 );
    if (event_on[i]) j+=1;
  }
#endif
}

void enable_tracing_( ) {
  enable_tracing( );
}
void initialize_tracing_( ) {
  initialize_tracing( );
}
void trace_event_( int count, int v ) {
  trace_event( count, v );
}
