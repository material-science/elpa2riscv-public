#include <stdlib.h>
#include <stdio.h>

#ifdef __epi
#include "riscv_macros.h"
#include "tracing.h"
void real_riscv_kernel_prload( double* q, double* hh[2], int nb, int nq );
#endif

extern int nops;

void real_riscv_prload( double *bcast_buffer, double* q, int ncols, int nbw, int nq ) {
  double *w[2];
  nops=0;
#ifdef __epi
  initialize_tracing();
  enable_tracing();
  trace_event(1000,1);

  for (int j=ncols-1; j>=0; j-=2) {
    w[0] = &bcast_buffer[j*nbw];
    w[1] = &bcast_buffer[(j-1)*nbw];
    real_riscv_kernel_prload( &q[(j-1)*nq], w, nbw, nq );
  }
  nops = (3*nbw + nq*(1+8*nbw))*ncols/2;

  trace_event(1000,0);
# endif
}
#ifdef __epi
void real_riscv_kernel_prload( double* q, double* hh[2], int nb, int nq ) {
  int i, j;

  double s = hh[1][1]*1.0;
  for (int i = 2; i < nb; i++)
    s += hh[0][i-1] * hh[1][i];

  for( i=0; i<nq; ) {
    DOUBLE_REG sign, X, Y, H1, H2, Q, TAU1, TAU2, S;
    unsigned long int vl = SET_VECTOR_LENGTH( nq-i );
    sign = VECTOR_SET(-1.0,vl);
    X = VECTOR_LOAD(&q[nq+i],vl);
    H1 = VECTOR_SET(hh[1][1],vl);

    Y = VECTOR_LOAD(q+i,vl);
    Y = MULTIPLY_ACC(Y,H1,X,vl );
    Q = VECTOR_LOAD(&q[2*nq+i],vl);
    for(j = 2; j < nb; j++) {
      H1 = VECTOR_SET(hh[0][j-1],vl);
      H2 = VECTOR_SET(hh[1][j],vl);
      X = MULTIPLY_ACC(X,H1,Q,vl);
      Y = MULTIPLY_ACC(Y,H2,Q,vl);
      Q = VECTOR_LOAD(&q[(j+1)*nq+i],vl);
    }
    H1 = VECTOR_SET(hh[0][nb-1],vl);
    X = MULTIPLY_ACC(X,H1,Q,vl);
    TAU1 = VECTOR_SET(hh[0][0],vl);
    TAU2 = VECTOR_SET(hh[1][0],vl);

    S = VECTOR_SET(s,vl);
    H1 = XOR_SIGN(TAU1,sign,vl);
    X = VECTOR_MULTIPLY(X,H1,vl);

    H1 = XOR_SIGN(TAU2,sign,vl);
    H2 = VECTOR_MULTIPLY(H1,S,vl);

    Y = VECTOR_MULTIPLY(Y,H1,vl);
    Y = MULTIPLY_ACC(Y,H2,X,vl);

    Q = VECTOR_LOAD(q+i,vl);
    Q = VECTOR_ADDITION(Q,Y,vl);

    VECTOR_STORE(q+i,Q,vl);
    H2 = VECTOR_SET( hh[1][1], vl );
    Q = VECTOR_LOAD( &q[nq+i], vl );
    Q = MULTIPLY_ACC( Q, H2, Y, vl );
    Q = VECTOR_ADDITION( Q, X, vl );
    VECTOR_STORE( &q[nq+i], Q, vl );

    for( j=2; j<nb; j++ ) {
      H1 = VECTOR_SET( hh[0][j-1], vl );
      H2 = VECTOR_SET( hh[1][j], vl );
      Q = VECTOR_LOAD( &q[j*nq+i], vl );
      Q = MULTIPLY_ACC( Q, H1, X, vl );
      Q = MULTIPLY_ACC( Q, H2, Y, vl );
      VECTOR_STORE( &q[j*nq+i], Q, vl );
    }
    H1 = VECTOR_SET( hh[0][nb-1], vl );
    Q = VECTOR_LOAD( &q[nb*nq+i], vl );
    Q = MULTIPLY_ACC( Q, H1, X, vl );
    VECTOR_STORE( &q[nb*nq+i], Q, vl );
    i += vl;
  }
}

# endif
