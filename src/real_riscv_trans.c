#include <stdlib.h>
#include <stdio.h>

#ifdef __epi
#include "riscv_macros.h"
#include "tracing.h"
void real_riscv_trans_kernel( double* q, double* hh[2], int nb, int nq );
#endif

extern int nops;

void real_riscv_trans( double *bcast_buffer, double* q, int ncols, int nbw, int nq ) {
  double *w[2];
  nops=0;
#ifdef __epi
  initialize_tracing();
  enable_tracing();
  trace_event(1000,1);

  for (int j=ncols-1; j>=0; j-=2) {
    w[0] = &bcast_buffer[j*nbw];
    w[1] = &bcast_buffer[(j-1)*nbw];
    real_riscv_trans_kernel( &q[(j-1)*nq], w, nbw, nq );
  }
  nops = (3*nbw + nq*(1+8*nbw))*ncols/2;

  trace_event(1000,0);
# endif
}

#ifdef __epi
void real_riscv_trans_kernel( double* q, double* hh[2], int nb, int nq ) {
  int max_vl=GET_MAX_VECTOR_LENGTH( );
  if (nb-2>max_vl) { printf( "Check this: NB is too large\n" ); exit(1); }


  int vl = SET_VECTOR_LENGTH(nb-2);
  DOUBLE_REG H1 = VECTOR_LOAD(&hh[0][1],vl);
  DOUBLE_REG H2 = VECTOR_LOAD(&hh[1][2],vl);
  DOUBLE_REG S = VECTOR_MULTIPLY(H1,H2,vl);
  DOUBLE_REG S0 = VECTOR_SET(hh[1][1],1);
  S = REDUCE_SUM(S,S0,vl);
  double s = VECTOR_GET(S);

  for (int i=0; i< nq; i++) {
    DOUBLE_REG Q = VECTOR_LOAD_STRIDED( &q[i+2*nq], nq*sizeof(double), vl );
    DOUBLE_REG X = VECTOR_MULTIPLY(Q,H1,vl);
    DOUBLE_REG Y = VECTOR_MULTIPLY(Q,H2,vl);
    DOUBLE_REG X0 = VECTOR_SET(q[i+nq]+q[i+nb*nq]*hh[0][nb-1],1);
    DOUBLE_REG Y0 = VECTOR_SET(q[i] + q[i+nq]*hh[1][1],1);
    X = REDUCE_SUM(X,X0,vl);
    Y = REDUCE_SUM(Y,Y0,vl);
    double x = VECTOR_GET(X);
    double y = VECTOR_GET(Y);

    double tau1 = hh[0][0];
    double tau2 = hh[1][0];
    double h1 = -tau1;

    x = x * h1;
    h1 = -tau2;
    double h2 = -tau2 * s;

    y = y*h1 + x*h2;

    q[i] = q[i] + y;
    q[i+nq] = q[i+nq] + x + y*hh[1][1];

    X = VECTOR_SET( x, vl );
    Y = VECTOR_SET( y, vl );
    Q = MULTIPLY_ACC(Q,H1,X,vl);
    Q = MULTIPLY_ACC(Q,H2,Y,vl);
    VECTOR_STORE_STRIDED( &q[i+2*nq], Q, nq*sizeof(double), vl );
    q[i+nb*nq] += x*hh[0][nb-1];
  }
}
# endif
