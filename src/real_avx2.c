#include <stdlib.h>
#include <stdio.h>

#ifdef _AVX2
#include <x86intrin.h>
#include "tracing.h"

__attribute__((always_inline)) static inline void hh_trafo_kernel_4_AVX2_2hv_double (double* q, double* hh, int nb, int ldq, int ldh,
 double s);
__attribute__((always_inline)) static inline void hh_trafo_kernel_8_AVX2_2hv_double (double* q, double* hh, int nb, int ldq, int ldh,
 double s);
__attribute__((always_inline)) static inline void hh_trafo_kernel_12_AVX2_2hv_double (double* q, double* hh, int nb, int ldq, int ldh,
 double s);
__attribute__((always_inline)) static inline void hh_trafo_kernel_16_AVX2_2hv_double (double* q, double* hh, int nb, int ldq, int ldh,
 double s);
__attribute__((always_inline)) static inline void hh_trafo_kernel_20_AVX2_2hv_double (double* q, double* hh, int nb, int ldq, int ldh,
 double s);
__attribute__((always_inline)) static inline void hh_trafo_kernel_24_AVX2_2hv_double (double* q, double* hh, int nb, int ldq, int ldh,
 double s);
void double_hh_trafo_real_AVX2_2hv_double (double* q, double* hh, int nb, int nq, int ldq, int ldh);
#endif

extern int nops;

void real_avx2( double *bcast_buffer, double* q, int ncols, int nbw, int nq ) {
  double *w;

  nops=0;
#ifdef _AVX2
  if (posix_memalign((void **)&w,64,2*nbw*sizeof(double))!=0) {
    printf( "posix_memalign error @real_avx2\n" );
    exit(0);
  }
  initialize_tracing();
  enable_tracing();
  trace_event(1000,1);
  int off=0;
  for (int i=0; i< nq; i+=48) {
    int lq = nq-i < 48 ? nq-i : 48;
    for (int j=ncols-1; j>=0; j-=2) {
      for (int k=0; k< nbw; k++) w[k]     = bcast_buffer[j*nbw+k];
      for (int k=0; k< nbw; k++) w[k+nbw] = bcast_buffer[(j-1)*nbw+k];
      double_hh_trafo_real_AVX2_2hv_double( &q[off+(j-1)*lq], w, nbw, lq, lq, nbw );
    }
    off += lq*(ncols+nbw);
  }
  nops = (nq*nbw*8-2*nq+4)*ncols/2 + 2*(nbw-2);
  trace_event(1000,0);
  free(w);
# endif
}

#ifdef _AVX2
void double_hh_trafo_real_AVX2_2hv_double (double* q, double* hh, int nb, int nq, int ldq, int ldh)
{
  int i;
  int worked_on;
  worked_on = 0;
  double s = hh[(ldh)+1]*1.0;
#pragma ivdep
  for (i = 2; i < nb; i++)
    {
      s += hh[i-1] * hh[(i+ldh)];
    }
  for (i = 0; i < nq - 20; i+= 24 )
    {
      hh_trafo_kernel_24_AVX2_2hv_double (&q[i], hh, nb, ldq, ldh, s);
      worked_on += 24;
    }
  if (nq == i)
    {
      return;
    }
  if (nq-i == 20)
    {
      hh_trafo_kernel_20_AVX2_2hv_double (&q[i], hh, nb, ldq, ldh, s);
      worked_on += 20;
    }
  if (nq-i == 16)
    {
      hh_trafo_kernel_16_AVX2_2hv_double (&q[i], hh, nb, ldq, ldh, s);
      worked_on += 16;
    }
  if (nq-i == 12)
    {
      hh_trafo_kernel_12_AVX2_2hv_double (&q[i], hh, nb, ldq, ldh, s);
      worked_on += 12;
    }
  if (nq-i == 8)
    {
      hh_trafo_kernel_8_AVX2_2hv_double (&q[i], hh, nb, ldq, ldh, s);
      worked_on += 8;
    }
  if (nq-i == 4)
    {
      hh_trafo_kernel_4_AVX2_2hv_double (&q[i], hh, nb, ldq, ldh, s);
      worked_on += 4;
    }
}

__attribute__((always_inline)) static inline void hh_trafo_kernel_24_AVX2_2hv_double (double* q, double* hh, int nb, int ldq, int ldh,
               double s)
  {
    int i;
    __m256d sign = (__m256d)_mm256_set1_epi64x(0x8000000000000000);
    __m256d x1 = _mm256_load_pd(&q[ldq]);
    __m256d x2 = _mm256_load_pd(&q[ldq+4]);
    __m256d x3 = _mm256_load_pd(&q[ldq+2*4]);
    __m256d x4 = _mm256_load_pd(&q[ldq+3*4]);
    __m256d x5 = _mm256_load_pd(&q[ldq+4*4]);
    __m256d x6 = _mm256_load_pd(&q[ldq+5*4]);
    __m256d h1 = _mm256_broadcast_sd(&hh[ldh+1]);
    __m256d h2;
    __m256d q1 = _mm256_load_pd(q);
    __m256d y1 = _mm256_add_pd( q1, _mm256_mul_pd( x1, h1));
    __m256d q2 = _mm256_load_pd(&q[4]);
    __m256d y2 = _mm256_add_pd( q2, _mm256_mul_pd( x2, h1));
    __m256d q3 = _mm256_load_pd(&q[2*4]);
    __m256d y3 = _mm256_add_pd( q3, _mm256_mul_pd( x3, h1));
    __m256d q4 = _mm256_load_pd(&q[3*4]);
    __m256d y4 = _mm256_add_pd( q4, _mm256_mul_pd( x4, h1));
    __m256d q5 = _mm256_load_pd(&q[4*4]);
    __m256d y5 = _mm256_add_pd( q5, _mm256_mul_pd( x5, h1));
    __m256d q6 = _mm256_load_pd(&q[5*4]);
    __m256d y6 = _mm256_add_pd( q6, _mm256_mul_pd( x6, h1));
    for(i = 2; i < nb; i++)
      {
        h1 = _mm256_broadcast_sd(&hh[i-(2 -1)]);
        h2 = _mm256_broadcast_sd(&hh[ldh+i-(2 -2)]);
        q1 = _mm256_load_pd(&q[i*ldq]);
        q2 = _mm256_load_pd(&q[(i*ldq)+4]);
        q3 = _mm256_load_pd(&q[(i*ldq)+2*4]);
        q4 = _mm256_load_pd(&q[(i*ldq)+3*4]);
        q5 = _mm256_load_pd(&q[(i*ldq)+4*4]);
        q6 = _mm256_load_pd(&q[(i*ldq)+5*4]);
        x1 = _mm256_add_pd( x1, _mm256_mul_pd( q1,h1));
        y1 = _mm256_add_pd( y1, _mm256_mul_pd( q1,h2));
        x2 = _mm256_add_pd( x2, _mm256_mul_pd( q2,h1));
        y2 = _mm256_add_pd( y2, _mm256_mul_pd( q2,h2));
        x3 = _mm256_add_pd( x3, _mm256_mul_pd( q3,h1));
        y3 = _mm256_add_pd( y3, _mm256_mul_pd( q3,h2));
        x4 = _mm256_add_pd( x4, _mm256_mul_pd( q4,h1));
        y4 = _mm256_add_pd( y4, _mm256_mul_pd( q4,h2));
        x5 = _mm256_add_pd( x5, _mm256_mul_pd( q5,h1));
        y5 = _mm256_add_pd( y5, _mm256_mul_pd( q5,h2));
        x6 = _mm256_add_pd( x6, _mm256_mul_pd( q6,h1));
        y6 = _mm256_add_pd( y6, _mm256_mul_pd( q6,h2));
      }
    h1 = _mm256_broadcast_sd(&hh[nb-(2 -1)]);
    q1 = _mm256_load_pd(&q[nb*ldq]);
    q2 = _mm256_load_pd(&q[(nb*ldq)+4]);
    q3 = _mm256_load_pd(&q[(nb*ldq)+2*4]);
    q4 = _mm256_load_pd(&q[(nb*ldq)+3*4]);
    q5 = _mm256_load_pd(&q[(nb*ldq)+4*4]);
    q6 = _mm256_load_pd(&q[(nb*ldq)+5*4]);
    x1 = _mm256_add_pd( x1, _mm256_mul_pd( q1,h1));
    x2 = _mm256_add_pd( x2, _mm256_mul_pd( q2,h1));
    x3 = _mm256_add_pd( x3, _mm256_mul_pd( q3,h1));
    x4 = _mm256_add_pd( x4, _mm256_mul_pd( q4,h1));
    x5 = _mm256_add_pd( x5, _mm256_mul_pd( q5,h1));
    x6 = _mm256_add_pd( x6, _mm256_mul_pd( q6,h1));
   __m256d tau1 = _mm256_broadcast_sd(hh);
   __m256d tau2 = _mm256_broadcast_sd(&hh[ldh]);
   __m256d vs = _mm256_broadcast_sd(&s);
    h1 = _mm256_xor_pd(tau1, sign);
   x1 = _mm256_mul_pd( x1, h1);
   x2 = _mm256_mul_pd( x2, h1);
   x3 = _mm256_mul_pd( x3, h1);
   x4 = _mm256_mul_pd( x4, h1);
   x5 = _mm256_mul_pd( x5, h1);
   x6 = _mm256_mul_pd( x6, h1);
   h1 = _mm256_xor_pd(tau2, sign);
   h2 = _mm256_mul_pd( h1, vs);
   y1 = _mm256_add_pd( _mm256_mul_pd( y1,h1), _mm256_mul_pd( x1,h2));
   y2 = _mm256_add_pd( _mm256_mul_pd( y2,h1), _mm256_mul_pd( x2,h2));
   y3 = _mm256_add_pd( _mm256_mul_pd( y3,h1), _mm256_mul_pd( x3,h2));
   y4 = _mm256_add_pd( _mm256_mul_pd( y4,h1), _mm256_mul_pd( x4,h2));
   y5 = _mm256_add_pd( _mm256_mul_pd( y5,h1), _mm256_mul_pd( x5,h2));
   y6 = _mm256_add_pd( _mm256_mul_pd( y6,h1), _mm256_mul_pd( x6,h2));
   q1 = _mm256_load_pd(&q[0]);
   q1 = _mm256_add_pd( q1, y1);
   _mm256_store_pd(&q[0], q1);
   q2 = _mm256_load_pd(&q[4]);
   q2 = _mm256_add_pd( q2, y2);
   _mm256_store_pd(&q[4], q2);
   q3 = _mm256_load_pd(&q[2*4]);
   q3 = _mm256_add_pd( q3, y3);
   _mm256_store_pd(&q[2*4], q3);
   q4 = _mm256_load_pd(&q[3*4]);
   q4 = _mm256_add_pd( q4, y4);
   _mm256_store_pd(&q[3*4], q4);
   q5 = _mm256_load_pd(&q[4*4]);
   q5 = _mm256_add_pd( q5, y5);
   _mm256_store_pd(&q[4*4], q5);
   q6 = _mm256_load_pd(&q[5*4]);
   q6 = _mm256_add_pd( q6, y6);
   _mm256_store_pd(&q[5*4], q6);
   h2 = _mm256_broadcast_sd(&hh[ldh+1]);
   q1 = _mm256_load_pd(&q[ldq]);
   q2 = _mm256_load_pd(&q[ldq+4]);
   q3 = _mm256_load_pd(&q[ldq+2*4]);
   q4 = _mm256_load_pd(&q[ldq+3*4]);
   q5 = _mm256_load_pd(&q[ldq+4*4]);
   q6 = _mm256_load_pd(&q[ldq+5*4]);
   q1 = _mm256_add_pd( q1, _mm256_add_pd( x1, _mm256_mul_pd( y1, h2)));
   q2 = _mm256_add_pd( q2, _mm256_add_pd( x2, _mm256_mul_pd( y2, h2)));
   q3 = _mm256_add_pd( q3, _mm256_add_pd( x3, _mm256_mul_pd( y3, h2)));
   q4 = _mm256_add_pd( q4, _mm256_add_pd( x4, _mm256_mul_pd( y4, h2)));
   q5 = _mm256_add_pd( q5, _mm256_add_pd( x5, _mm256_mul_pd( y5, h2)));
   q6 = _mm256_add_pd( q6, _mm256_add_pd( x6, _mm256_mul_pd( y6, h2)));
   _mm256_store_pd(&q[ldq], q1);
   _mm256_store_pd(&q[ldq+4], q2);
   _mm256_store_pd(&q[ldq+2*4], q3);
   _mm256_store_pd(&q[ldq+3*4], q4);
   _mm256_store_pd(&q[ldq+4*4], q5);
   _mm256_store_pd(&q[ldq+5*4], q6);
   for (i = 2; i < nb; i++)
   {
    h1 = _mm256_broadcast_sd(&hh[i-(2 -1)]);
    h2 = _mm256_broadcast_sd(&hh[ldh+i-(2 -2)]);
     q1 = _mm256_load_pd(&q[i*ldq]);
     q2 = _mm256_load_pd(&q[(i*ldq)+4]);
     q3 = _mm256_load_pd(&q[(i*ldq)+2*4]);
     q4 = _mm256_load_pd(&q[(i*ldq)+3*4]);
     q5 = _mm256_load_pd(&q[(i*ldq)+4*4]);
     q6 = _mm256_load_pd(&q[(i*ldq)+5*4]);
     q1 = _mm256_add_pd( q1, _mm256_add_pd( _mm256_mul_pd( x1,h1), _mm256_mul_pd( y1, h2)));
     q2 = _mm256_add_pd( q2, _mm256_add_pd( _mm256_mul_pd( x2,h1), _mm256_mul_pd( y2, h2)));
     q3 = _mm256_add_pd( q3, _mm256_add_pd( _mm256_mul_pd( x3,h1), _mm256_mul_pd( y3, h2)));
     q4 = _mm256_add_pd( q4, _mm256_add_pd( _mm256_mul_pd( x4,h1), _mm256_mul_pd( y4, h2)));
     q5 = _mm256_add_pd( q5, _mm256_add_pd( _mm256_mul_pd( x5,h1), _mm256_mul_pd( y5, h2)));
     q6 = _mm256_add_pd( q6, _mm256_add_pd( _mm256_mul_pd( x6,h1), _mm256_mul_pd( y6, h2)));
     _mm256_store_pd(&q[i*ldq], q1);
     _mm256_store_pd(&q[(i*ldq)+4], q2);
     _mm256_store_pd(&q[(i*ldq)+2*4], q3);
     _mm256_store_pd(&q[(i*ldq)+3*4], q4);
     _mm256_store_pd(&q[(i*ldq)+4*4], q5);
     _mm256_store_pd(&q[(i*ldq)+5*4], q6);
   }
   h1 = _mm256_broadcast_sd(&hh[nb-(2 -1)]);
   q1 = _mm256_load_pd(&q[nb*ldq]);
   q2 = _mm256_load_pd(&q[(nb*ldq)+4]);
   q3 = _mm256_load_pd(&q[(nb*ldq)+2*4]);
   q4 = _mm256_load_pd(&q[(nb*ldq)+3*4]);
   q5 = _mm256_load_pd(&q[(nb*ldq)+4*4]);
   q6 = _mm256_load_pd(&q[(nb*ldq)+5*4]);
   q1 = _mm256_add_pd( q1, _mm256_mul_pd( x1, h1));
   q2 = _mm256_add_pd( q2, _mm256_mul_pd( x2, h1));
   q3 = _mm256_add_pd( q3, _mm256_mul_pd( x3, h1));
   q4 = _mm256_add_pd( q4, _mm256_mul_pd( x4, h1));
   q5 = _mm256_add_pd( q5, _mm256_mul_pd( x5, h1));
   q6 = _mm256_add_pd( q6, _mm256_mul_pd( x6, h1));
   _mm256_store_pd(&q[nb*ldq], q1);
   _mm256_store_pd(&q[(nb*ldq)+4], q2);
   _mm256_store_pd(&q[(nb*ldq)+2*4], q3);
   _mm256_store_pd(&q[(nb*ldq)+3*4], q4);
   _mm256_store_pd(&q[(nb*ldq)+4*4], q5);
   _mm256_store_pd(&q[(nb*ldq)+5*4], q6);
}
__attribute__((always_inline)) static inline void hh_trafo_kernel_20_AVX2_2hv_double (double* q, double* hh, int nb, int ldq, int ldh,
               double s)
  {
    int i;
        __m256d sign = (__m256d)_mm256_set1_epi64x(0x8000000000000000);
    __m256d x1 = _mm256_load_pd(&q[ldq]);
    __m256d x2 = _mm256_load_pd(&q[ldq+4]);
    __m256d x3 = _mm256_load_pd(&q[ldq+2*4]);
    __m256d x4 = _mm256_load_pd(&q[ldq+3*4]);
    __m256d x5 = _mm256_load_pd(&q[ldq+4*4]);
    __m256d h1 = _mm256_broadcast_sd(&hh[ldh+1]);
    __m256d h2;
    __m256d q1 = _mm256_load_pd(q);
    __m256d y1 = _mm256_add_pd( q1, _mm256_mul_pd( x1, h1));
    __m256d q2 = _mm256_load_pd(&q[4]);
    __m256d y2 = _mm256_add_pd( q2, _mm256_mul_pd( x2, h1));
    __m256d q3 = _mm256_load_pd(&q[2*4]);
    __m256d y3 = _mm256_add_pd( q3, _mm256_mul_pd( x3, h1));
    __m256d q4 = _mm256_load_pd(&q[3*4]);
    __m256d y4 = _mm256_add_pd( q4, _mm256_mul_pd( x4, h1));
    __m256d q5 = _mm256_load_pd(&q[4*4]);
    __m256d y5 = _mm256_add_pd( q5, _mm256_mul_pd( x5, h1));
    for(i = 2; i < nb; i++)
      {
        h1 = _mm256_broadcast_sd(&hh[i-(2 -1)]);
        h2 = _mm256_broadcast_sd(&hh[ldh+i-(2 -2)]);
        q1 = _mm256_load_pd(&q[i*ldq]);
        q2 = _mm256_load_pd(&q[(i*ldq)+4]);
        q3 = _mm256_load_pd(&q[(i*ldq)+2*4]);
        q4 = _mm256_load_pd(&q[(i*ldq)+3*4]);
        q5 = _mm256_load_pd(&q[(i*ldq)+4*4]);
        x1 = _mm256_add_pd( x1, _mm256_mul_pd( q1,h1));
        y1 = _mm256_add_pd( y1, _mm256_mul_pd( q1,h2));
        x2 = _mm256_add_pd( x2, _mm256_mul_pd( q2,h1));
        y2 = _mm256_add_pd( y2, _mm256_mul_pd( q2,h2));
        x3 = _mm256_add_pd( x3, _mm256_mul_pd( q3,h1));
        y3 = _mm256_add_pd( y3, _mm256_mul_pd( q3,h2));
        x4 = _mm256_add_pd( x4, _mm256_mul_pd( q4,h1));
        y4 = _mm256_add_pd( y4, _mm256_mul_pd( q4,h2));
        x5 = _mm256_add_pd( x5, _mm256_mul_pd( q5,h1));
        y5 = _mm256_add_pd( y5, _mm256_mul_pd( q5,h2));
      }
    h1 = _mm256_broadcast_sd(&hh[nb-(2 -1)]);
    q1 = _mm256_load_pd(&q[nb*ldq]);
    q2 = _mm256_load_pd(&q[(nb*ldq)+4]);
    q3 = _mm256_load_pd(&q[(nb*ldq)+2*4]);
    q4 = _mm256_load_pd(&q[(nb*ldq)+3*4]);
    q5 = _mm256_load_pd(&q[(nb*ldq)+4*4]);
    x1 = _mm256_add_pd( x1, _mm256_mul_pd( q1,h1));
    x2 = _mm256_add_pd( x2, _mm256_mul_pd( q2,h1));
    x3 = _mm256_add_pd( x3, _mm256_mul_pd( q3,h1));
    x4 = _mm256_add_pd( x4, _mm256_mul_pd( q4,h1));
    x5 = _mm256_add_pd( x5, _mm256_mul_pd( q5,h1));
   __m256d tau1 = _mm256_broadcast_sd(hh);
   __m256d tau2 = _mm256_broadcast_sd(&hh[ldh]);
   __m256d vs = _mm256_broadcast_sd(&s);
    h1 = _mm256_xor_pd(tau1, sign);
   x1 = _mm256_mul_pd( x1, h1);
   x2 = _mm256_mul_pd( x2, h1);
   x3 = _mm256_mul_pd( x3, h1);
   x4 = _mm256_mul_pd( x4, h1);
   x5 = _mm256_mul_pd( x5, h1);
   h1 = _mm256_xor_pd(tau2, sign);
   h2 = _mm256_mul_pd( h1, vs);
   y1 = _mm256_add_pd( _mm256_mul_pd( y1,h1), _mm256_mul_pd( x1,h2));
   y2 = _mm256_add_pd( _mm256_mul_pd( y2,h1), _mm256_mul_pd( x2,h2));
   y3 = _mm256_add_pd( _mm256_mul_pd( y3,h1), _mm256_mul_pd( x3,h2));
   y4 = _mm256_add_pd( _mm256_mul_pd( y4,h1), _mm256_mul_pd( x4,h2));
   y5 = _mm256_add_pd( _mm256_mul_pd( y5,h1), _mm256_mul_pd( x5,h2));
   q1 = _mm256_load_pd(&q[0]);
   q1 = _mm256_add_pd( q1, y1);
   _mm256_store_pd(&q[0], q1);
   q2 = _mm256_load_pd(&q[4]);
   q2 = _mm256_add_pd( q2, y2);
   _mm256_store_pd(&q[4], q2);
   q3 = _mm256_load_pd(&q[2*4]);
   q3 = _mm256_add_pd( q3, y3);
   _mm256_store_pd(&q[2*4], q3);
   q4 = _mm256_load_pd(&q[3*4]);
   q4 = _mm256_add_pd( q4, y4);
   _mm256_store_pd(&q[3*4], q4);
   q5 = _mm256_load_pd(&q[4*4]);
   q5 = _mm256_add_pd( q5, y5);
   _mm256_store_pd(&q[4*4], q5);
   h2 = _mm256_broadcast_sd(&hh[ldh+1]);
   q1 = _mm256_load_pd(&q[ldq]);
   q2 = _mm256_load_pd(&q[ldq+4]);
   q3 = _mm256_load_pd(&q[ldq+2*4]);
   q4 = _mm256_load_pd(&q[ldq+3*4]);
   q5 = _mm256_load_pd(&q[ldq+4*4]);
   q1 = _mm256_add_pd( q1, _mm256_add_pd( x1, _mm256_mul_pd( y1, h2)));
   q2 = _mm256_add_pd( q2, _mm256_add_pd( x2, _mm256_mul_pd( y2, h2)));
   q3 = _mm256_add_pd( q3, _mm256_add_pd( x3, _mm256_mul_pd( y3, h2)));
   q4 = _mm256_add_pd( q4, _mm256_add_pd( x4, _mm256_mul_pd( y4, h2)));
   q5 = _mm256_add_pd( q5, _mm256_add_pd( x5, _mm256_mul_pd( y5, h2)));
   _mm256_store_pd(&q[ldq], q1);
   _mm256_store_pd(&q[ldq+4], q2);
   _mm256_store_pd(&q[ldq+2*4], q3);
   _mm256_store_pd(&q[ldq+3*4], q4);
   _mm256_store_pd(&q[ldq+4*4], q5);
   for (i = 2; i < nb; i++)
   {
    h1 = _mm256_broadcast_sd(&hh[i-(2 -1)]);
    h2 = _mm256_broadcast_sd(&hh[ldh+i-(2 -2)]);
     q1 = _mm256_load_pd(&q[i*ldq]);
     q2 = _mm256_load_pd(&q[(i*ldq)+4]);
     q3 = _mm256_load_pd(&q[(i*ldq)+2*4]);
     q4 = _mm256_load_pd(&q[(i*ldq)+3*4]);
     q5 = _mm256_load_pd(&q[(i*ldq)+4*4]);
     q1 = _mm256_add_pd( q1, _mm256_add_pd( _mm256_mul_pd( x1,h1), _mm256_mul_pd( y1, h2)));
     q2 = _mm256_add_pd( q2, _mm256_add_pd( _mm256_mul_pd( x2,h1), _mm256_mul_pd( y2, h2)));
     q3 = _mm256_add_pd( q3, _mm256_add_pd( _mm256_mul_pd( x3,h1), _mm256_mul_pd( y3, h2)));
     q4 = _mm256_add_pd( q4, _mm256_add_pd( _mm256_mul_pd( x4,h1), _mm256_mul_pd( y4, h2)));
     q5 = _mm256_add_pd( q5, _mm256_add_pd( _mm256_mul_pd( x5,h1), _mm256_mul_pd( y5, h2)));
     _mm256_store_pd(&q[i*ldq], q1);
     _mm256_store_pd(&q[(i*ldq)+4], q2);
     _mm256_store_pd(&q[(i*ldq)+2*4], q3);
     _mm256_store_pd(&q[(i*ldq)+3*4], q4);
     _mm256_store_pd(&q[(i*ldq)+4*4], q5);
   }
   h1 = _mm256_broadcast_sd(&hh[nb-(2 -1)]);
   q1 = _mm256_load_pd(&q[nb*ldq]);
   q2 = _mm256_load_pd(&q[(nb*ldq)+4]);
   q3 = _mm256_load_pd(&q[(nb*ldq)+2*4]);
   q4 = _mm256_load_pd(&q[(nb*ldq)+3*4]);
   q5 = _mm256_load_pd(&q[(nb*ldq)+4*4]);
   q1 = _mm256_add_pd( q1, _mm256_mul_pd( x1, h1));
   q2 = _mm256_add_pd( q2, _mm256_mul_pd( x2, h1));
   q3 = _mm256_add_pd( q3, _mm256_mul_pd( x3, h1));
   q4 = _mm256_add_pd( q4, _mm256_mul_pd( x4, h1));
   q5 = _mm256_add_pd( q5, _mm256_mul_pd( x5, h1));
   _mm256_store_pd(&q[nb*ldq], q1);
   _mm256_store_pd(&q[(nb*ldq)+4], q2);
   _mm256_store_pd(&q[(nb*ldq)+2*4], q3);
   _mm256_store_pd(&q[(nb*ldq)+3*4], q4);
   _mm256_store_pd(&q[(nb*ldq)+4*4], q5);
}
__attribute__((always_inline)) static inline void hh_trafo_kernel_16_AVX2_2hv_double (double* q, double* hh, int nb, int ldq, int ldh,
               double s)
  {
    int i;
        __m256d sign = (__m256d)_mm256_set1_epi64x(0x8000000000000000);
    __m256d x1 = _mm256_load_pd(&q[ldq]);
    __m256d x2 = _mm256_load_pd(&q[ldq+4]);
    __m256d x3 = _mm256_load_pd(&q[ldq+2*4]);
    __m256d x4 = _mm256_load_pd(&q[ldq+3*4]);
    __m256d h1 = _mm256_broadcast_sd(&hh[ldh+1]);
    __m256d h2;
    __m256d q1 = _mm256_load_pd(q);
    __m256d y1 = _mm256_add_pd( q1, _mm256_mul_pd( x1, h1));
    __m256d q2 = _mm256_load_pd(&q[4]);
    __m256d y2 = _mm256_add_pd( q2, _mm256_mul_pd( x2, h1));
    __m256d q3 = _mm256_load_pd(&q[2*4]);
    __m256d y3 = _mm256_add_pd( q3, _mm256_mul_pd( x3, h1));
    __m256d q4 = _mm256_load_pd(&q[3*4]);
    __m256d y4 = _mm256_add_pd( q4, _mm256_mul_pd( x4, h1));
    for(i = 2; i < nb; i++)
      {
        h1 = _mm256_broadcast_sd(&hh[i-(2 -1)]);
        h2 = _mm256_broadcast_sd(&hh[ldh+i-(2 -2)]);
        q1 = _mm256_load_pd(&q[i*ldq]);
        q2 = _mm256_load_pd(&q[(i*ldq)+4]);
        q3 = _mm256_load_pd(&q[(i*ldq)+2*4]);
        q4 = _mm256_load_pd(&q[(i*ldq)+3*4]);
        x1 = _mm256_add_pd( x1, _mm256_mul_pd( q1,h1));
        y1 = _mm256_add_pd( y1, _mm256_mul_pd( q1,h2));
        x2 = _mm256_add_pd( x2, _mm256_mul_pd( q2,h1));
        y2 = _mm256_add_pd( y2, _mm256_mul_pd( q2,h2));
        x3 = _mm256_add_pd( x3, _mm256_mul_pd( q3,h1));
        y3 = _mm256_add_pd( y3, _mm256_mul_pd( q3,h2));
        x4 = _mm256_add_pd( x4, _mm256_mul_pd( q4,h1));
        y4 = _mm256_add_pd( y4, _mm256_mul_pd( q4,h2));
      }
    h1 = _mm256_broadcast_sd(&hh[nb-(2 -1)]);
    q1 = _mm256_load_pd(&q[nb*ldq]);
    q2 = _mm256_load_pd(&q[(nb*ldq)+4]);
    q3 = _mm256_load_pd(&q[(nb*ldq)+2*4]);
    q4 = _mm256_load_pd(&q[(nb*ldq)+3*4]);
    x1 = _mm256_add_pd( x1, _mm256_mul_pd( q1,h1));
    x2 = _mm256_add_pd( x2, _mm256_mul_pd( q2,h1));
    x3 = _mm256_add_pd( x3, _mm256_mul_pd( q3,h1));
    x4 = _mm256_add_pd( x4, _mm256_mul_pd( q4,h1));
   __m256d tau1 = _mm256_broadcast_sd(hh);
   __m256d tau2 = _mm256_broadcast_sd(&hh[ldh]);
   __m256d vs = _mm256_broadcast_sd(&s);
    h1 = _mm256_xor_pd(tau1, sign);
   x1 = _mm256_mul_pd( x1, h1);
   x2 = _mm256_mul_pd( x2, h1);
   x3 = _mm256_mul_pd( x3, h1);
   x4 = _mm256_mul_pd( x4, h1);
   h1 = _mm256_xor_pd(tau2, sign);
   h2 = _mm256_mul_pd( h1, vs);
   y1 = _mm256_add_pd( _mm256_mul_pd( y1,h1), _mm256_mul_pd( x1,h2));
   y2 = _mm256_add_pd( _mm256_mul_pd( y2,h1), _mm256_mul_pd( x2,h2));
   y3 = _mm256_add_pd( _mm256_mul_pd( y3,h1), _mm256_mul_pd( x3,h2));
   y4 = _mm256_add_pd( _mm256_mul_pd( y4,h1), _mm256_mul_pd( x4,h2));
   q1 = _mm256_load_pd(&q[0]);
   q2 = _mm256_load_pd(&q[4]);
   q3 = _mm256_load_pd(&q[2*4]);
   q4 = _mm256_load_pd(&q[3*4]);
   q1 = _mm256_add_pd( q1, y1);
   q2 = _mm256_add_pd( q2, y2);
   q3 = _mm256_add_pd( q3, y3);
   q4 = _mm256_add_pd( q4, y4);
   _mm256_store_pd(&q[0], q1);
   _mm256_store_pd(&q[4], q2);
   _mm256_store_pd(&q[2*4], q3);
   _mm256_store_pd(&q[3*4], q4);
   h2 = _mm256_broadcast_sd(&hh[ldh+1]);
   q1 = _mm256_load_pd(&q[ldq]);
   q2 = _mm256_load_pd(&q[ldq+4]);
   q3 = _mm256_load_pd(&q[ldq+2*4]);
   q4 = _mm256_load_pd(&q[ldq+3*4]);
   q1 = _mm256_add_pd( q1, _mm256_add_pd( x1, _mm256_mul_pd( y1, h2)));
   q2 = _mm256_add_pd( q2, _mm256_add_pd( x2, _mm256_mul_pd( y2, h2)));
   q3 = _mm256_add_pd( q3, _mm256_add_pd( x3, _mm256_mul_pd( y3, h2)));
   q4 = _mm256_add_pd( q4, _mm256_add_pd( x4, _mm256_mul_pd( y4, h2)));
   _mm256_store_pd(&q[ldq], q1);
   _mm256_store_pd(&q[ldq+4], q2);
   _mm256_store_pd(&q[ldq+2*4], q3);
   _mm256_store_pd(&q[ldq+3*4], q4);
   for (i = 2; i < nb; i++)
   {
    h1 = _mm256_broadcast_sd(&hh[i-(2 -1)]);
    h2 = _mm256_broadcast_sd(&hh[ldh+i-(2 -2)]);
     q1 = _mm256_load_pd(&q[i*ldq]);
     q2 = _mm256_load_pd(&q[(i*ldq)+4]);
     q3 = _mm256_load_pd(&q[(i*ldq)+2*4]);
     q4 = _mm256_load_pd(&q[(i*ldq)+3*4]);
     q1 = _mm256_add_pd( q1, _mm256_add_pd( _mm256_mul_pd( x1,h1), _mm256_mul_pd( y1, h2)));
     q2 = _mm256_add_pd( q2, _mm256_add_pd( _mm256_mul_pd( x2,h1), _mm256_mul_pd( y2, h2)));
     q3 = _mm256_add_pd( q3, _mm256_add_pd( _mm256_mul_pd( x3,h1), _mm256_mul_pd( y3, h2)));
     q4 = _mm256_add_pd( q4, _mm256_add_pd( _mm256_mul_pd( x4,h1), _mm256_mul_pd( y4, h2)));
     _mm256_store_pd(&q[i*ldq], q1);
     _mm256_store_pd(&q[(i*ldq)+4], q2);
     _mm256_store_pd(&q[(i*ldq)+2*4], q3);
     _mm256_store_pd(&q[(i*ldq)+3*4], q4);
   }
   h1 = _mm256_broadcast_sd(&hh[nb-(2 -1)]);
   q1 = _mm256_load_pd(&q[nb*ldq]);
   q2 = _mm256_load_pd(&q[(nb*ldq)+4]);
   q3 = _mm256_load_pd(&q[(nb*ldq)+2*4]);
   q4 = _mm256_load_pd(&q[(nb*ldq)+3*4]);
   q1 = _mm256_add_pd( q1, _mm256_mul_pd( x1, h1));
   q2 = _mm256_add_pd( q2, _mm256_mul_pd( x2, h1));
   q3 = _mm256_add_pd( q3, _mm256_mul_pd( x3, h1));
   q4 = _mm256_add_pd( q4, _mm256_mul_pd( x4, h1));
   _mm256_store_pd(&q[nb*ldq], q1);
   _mm256_store_pd(&q[(nb*ldq)+4], q2);
   _mm256_store_pd(&q[(nb*ldq)+2*4], q3);
   _mm256_store_pd(&q[(nb*ldq)+3*4], q4);
}
__attribute__((always_inline)) static inline void hh_trafo_kernel_12_AVX2_2hv_double (double* q, double* hh, int nb, int ldq, int ldh,
               double s)
  {
    int i;
        __m256d sign = (__m256d)_mm256_set1_epi64x(0x8000000000000000);
    __m256d x1 = _mm256_load_pd(&q[ldq]);
    __m256d x2 = _mm256_load_pd(&q[ldq+4]);
    __m256d x3 = _mm256_load_pd(&q[ldq+2*4]);
    __m256d h1 = _mm256_broadcast_sd(&hh[ldh+1]);
    __m256d h2;
    __m256d q1 = _mm256_load_pd(q);
    __m256d q2 = _mm256_load_pd(&q[4]);
    __m256d q3 = _mm256_load_pd(&q[2*4]);
    __m256d y1 = _mm256_add_pd( q1, _mm256_mul_pd( x1, h1));
    __m256d y2 = _mm256_add_pd( q2, _mm256_mul_pd( x2, h1));
    __m256d y3 = _mm256_add_pd( q3, _mm256_mul_pd( x3, h1));
    for(i = 2; i < nb; i++)
      {
        h1 = _mm256_broadcast_sd(&hh[i-(2 -1)]);
        h2 = _mm256_broadcast_sd(&hh[ldh+i-(2 -2)]);
        q1 = _mm256_load_pd(&q[i*ldq]);
        q2 = _mm256_load_pd(&q[(i*ldq)+4]);
        q3 = _mm256_load_pd(&q[(i*ldq)+2*4]);
        x1 = _mm256_add_pd( x1, _mm256_mul_pd( q1,h1));
        y1 = _mm256_add_pd( y1, _mm256_mul_pd( q1,h2));
        x2 = _mm256_add_pd( x2, _mm256_mul_pd( q2,h1));
        y2 = _mm256_add_pd( y2, _mm256_mul_pd( q2,h2));
        x3 = _mm256_add_pd( x3, _mm256_mul_pd( q3,h1));
        y3 = _mm256_add_pd( y3, _mm256_mul_pd( q3,h2));
      }
    h1 = _mm256_broadcast_sd(&hh[nb-(2 -1)]);
    q1 = _mm256_load_pd(&q[nb*ldq]);
    q2 = _mm256_load_pd(&q[(nb*ldq)+4]);
    q3 = _mm256_load_pd(&q[(nb*ldq)+2*4]);
    x1 = _mm256_add_pd( x1, _mm256_mul_pd( q1,h1));
    x2 = _mm256_add_pd( x2, _mm256_mul_pd( q2,h1));
    x3 = _mm256_add_pd( x3, _mm256_mul_pd( q3,h1));
   __m256d tau1 = _mm256_broadcast_sd(hh);
   __m256d tau2 = _mm256_broadcast_sd(&hh[ldh]);
   __m256d vs = _mm256_broadcast_sd(&s);
    h1 = _mm256_xor_pd(tau1, sign);
   x1 = _mm256_mul_pd( x1, h1);
   x2 = _mm256_mul_pd( x2, h1);
   x3 = _mm256_mul_pd( x3, h1);
   h1 = _mm256_xor_pd(tau2, sign);
   h2 = _mm256_mul_pd( h1, vs);
   y1 = _mm256_add_pd( _mm256_mul_pd( y1,h1), _mm256_mul_pd( x1,h2));
   y2 = _mm256_add_pd( _mm256_mul_pd( y2,h1), _mm256_mul_pd( x2,h2));
   y3 = _mm256_add_pd( _mm256_mul_pd( y3,h1), _mm256_mul_pd( x3,h2));
   q1 = _mm256_load_pd(&q[0]);
   q1 = _mm256_add_pd( q1, y1);
   _mm256_store_pd(&q[0], q1);
   q2 = _mm256_load_pd(&q[4]);
   q2 = _mm256_add_pd( q2, y2);
   _mm256_store_pd(&q[4], q2);
   q3 = _mm256_load_pd(&q[2*4]);
   q3 = _mm256_add_pd( q3, y3);
   _mm256_store_pd(&q[2*4], q3);
   h2 = _mm256_broadcast_sd(&hh[ldh+1]);
   q1 = _mm256_load_pd(&q[ldq]);
   q2 = _mm256_load_pd(&q[ldq+4]);
   q3 = _mm256_load_pd(&q[ldq+2*4]);
   q1 = _mm256_add_pd( q1, _mm256_add_pd( x1, _mm256_mul_pd( y1, h2)));
   q2 = _mm256_add_pd( q2, _mm256_add_pd( x2, _mm256_mul_pd( y2, h2)));
   q3 = _mm256_add_pd( q3, _mm256_add_pd( x3, _mm256_mul_pd( y3, h2)));
   _mm256_store_pd(&q[ldq], q1);
   _mm256_store_pd(&q[ldq+4], q2);
   _mm256_store_pd(&q[ldq+2*4], q3);
   for (i = 2; i < nb; i++)
   {
    h1 = _mm256_broadcast_sd(&hh[i-(2 -1)]);
    h2 = _mm256_broadcast_sd(&hh[ldh+i-(2 -2)]);
     q1 = _mm256_load_pd(&q[i*ldq]);
     q2 = _mm256_load_pd(&q[(i*ldq)+4]);
     q3 = _mm256_load_pd(&q[(i*ldq)+2*4]);
     q1 = _mm256_add_pd( q1, _mm256_add_pd( _mm256_mul_pd( x1,h1), _mm256_mul_pd( y1, h2)));
     q2 = _mm256_add_pd( q2, _mm256_add_pd( _mm256_mul_pd( x2,h1), _mm256_mul_pd( y2, h2)));
     q3 = _mm256_add_pd( q3, _mm256_add_pd( _mm256_mul_pd( x3,h1), _mm256_mul_pd( y3, h2)));
     _mm256_store_pd(&q[i*ldq], q1);
     _mm256_store_pd(&q[(i*ldq)+4], q2);
     _mm256_store_pd(&q[(i*ldq)+2*4], q3);
   }
   h1 = _mm256_broadcast_sd(&hh[nb-(2 -1)]);
   q1 = _mm256_load_pd(&q[nb*ldq]);
   q2 = _mm256_load_pd(&q[(nb*ldq)+4]);
   q3 = _mm256_load_pd(&q[(nb*ldq)+2*4]);
   q1 = _mm256_add_pd( q1, _mm256_mul_pd( x1, h1));
   q2 = _mm256_add_pd( q2, _mm256_mul_pd( x2, h1));
   q3 = _mm256_add_pd( q3, _mm256_mul_pd( x3, h1));
   _mm256_store_pd(&q[nb*ldq], q1);
   _mm256_store_pd(&q[(nb*ldq)+4], q2);
   _mm256_store_pd(&q[(nb*ldq)+2*4], q3);
}
__attribute__((always_inline)) static inline void hh_trafo_kernel_8_AVX2_2hv_double (double* q, double* hh, int nb, int ldq, int ldh,
               double s)
  {
    int i;
        __m256d sign = (__m256d)_mm256_set1_epi64x(0x8000000000000000);
    __m256d x1 = _mm256_load_pd(&q[ldq]);
    __m256d x2 = _mm256_load_pd(&q[ldq+4]);
    __m256d h1 = _mm256_broadcast_sd(&hh[ldh+1]);
    __m256d h2;
    __m256d q1 = _mm256_load_pd(q);
    __m256d y1 = _mm256_add_pd( q1, _mm256_mul_pd( x1, h1));
    __m256d q2 = _mm256_load_pd(&q[4]);
    __m256d y2 = _mm256_add_pd( q2, _mm256_mul_pd( x2, h1));
    for(i = 2; i < nb; i++)
      {
        h1 = _mm256_broadcast_sd(&hh[i-(2 -1)]);
        h2 = _mm256_broadcast_sd(&hh[ldh+i-(2 -2)]);
        q1 = _mm256_load_pd(&q[i*ldq]);
        q2 = _mm256_load_pd(&q[(i*ldq)+4]);
        x1 = _mm256_add_pd( x1, _mm256_mul_pd( q1,h1));
        y1 = _mm256_add_pd( y1, _mm256_mul_pd( q1,h2));
        x2 = _mm256_add_pd( x2, _mm256_mul_pd( q2,h1));
        y2 = _mm256_add_pd( y2, _mm256_mul_pd( q2,h2));
      }
    h1 = _mm256_broadcast_sd(&hh[nb-(2 -1)]);
    q1 = _mm256_load_pd(&q[nb*ldq]);
    q2 = _mm256_load_pd(&q[(nb*ldq)+4]);
    x1 = _mm256_add_pd( x1, _mm256_mul_pd( q1,h1));
    x2 = _mm256_add_pd( x2, _mm256_mul_pd( q2,h1));
   __m256d tau1 = _mm256_broadcast_sd(hh);
   __m256d tau2 = _mm256_broadcast_sd(&hh[ldh]);
   __m256d vs = _mm256_broadcast_sd(&s);
    h1 = _mm256_xor_pd(tau1, sign);
   x1 = _mm256_mul_pd( x1, h1);
   x2 = _mm256_mul_pd( x2, h1);
   h1 = _mm256_xor_pd(tau2, sign);
   h2 = _mm256_mul_pd( h1, vs);
   y1 = _mm256_add_pd( _mm256_mul_pd( y1,h1), _mm256_mul_pd( x1,h2));
   y2 = _mm256_add_pd( _mm256_mul_pd( y2,h1), _mm256_mul_pd( x2,h2));
   q1 = _mm256_load_pd(&q[0]);
   q1 = _mm256_add_pd( q1, y1);
   _mm256_store_pd(&q[0], q1);
   q2 = _mm256_load_pd(&q[4]);
   q2 = _mm256_add_pd( q2, y2);
   _mm256_store_pd(&q[4], q2);
   h2 = _mm256_broadcast_sd(&hh[ldh+1]);
   q1 = _mm256_load_pd(&q[ldq]);
   q1 = _mm256_add_pd( q1, _mm256_add_pd( x1, _mm256_mul_pd( y1, h2)));
   _mm256_store_pd(&q[ldq], q1);
   q2 = _mm256_load_pd(&q[ldq+4]);
   q2 = _mm256_add_pd( q2, _mm256_add_pd( x2, _mm256_mul_pd( y2, h2)));
   _mm256_store_pd(&q[ldq+4], q2);
   for (i = 2; i < nb; i++)
   {
    h1 = _mm256_broadcast_sd(&hh[i-(2 -1)]);
    h2 = _mm256_broadcast_sd(&hh[ldh+i-(2 -2)]);
     q1 = _mm256_load_pd(&q[i*ldq]);
     q2 = _mm256_load_pd(&q[(i*ldq)+4]);
     q1 = _mm256_add_pd( q1, _mm256_add_pd( _mm256_mul_pd( x1,h1), _mm256_mul_pd( y1, h2)));
     q2 = _mm256_add_pd( q2, _mm256_add_pd( _mm256_mul_pd( x2,h1), _mm256_mul_pd( y2, h2)));
     _mm256_store_pd(&q[i*ldq], q1);
     _mm256_store_pd(&q[(i*ldq)+4], q2);
   }
   h1 = _mm256_broadcast_sd(&hh[nb-(2 -1)]);
   q1 = _mm256_load_pd(&q[nb*ldq]);
   q2 = _mm256_load_pd(&q[(nb*ldq)+4]);
   q1 = _mm256_add_pd( q1, _mm256_mul_pd( x1, h1));
   q2 = _mm256_add_pd( q2, _mm256_mul_pd( x2, h1));
   _mm256_store_pd(&q[nb*ldq], q1);
   _mm256_store_pd(&q[(nb*ldq)+4], q2);
}
__attribute__((always_inline)) static inline void hh_trafo_kernel_4_AVX2_2hv_double (double* q, double* hh, int nb, int ldq, int ldh,
               double s)
  {
    int i;
        __m256d sign = (__m256d)_mm256_set1_epi64x(0x8000000000000000);
    __m256d x1 = _mm256_load_pd(&q[ldq]);
    __m256d h1 = _mm256_broadcast_sd(&hh[ldh+1]);
    __m256d h2;
    __m256d q1 = _mm256_load_pd(q);
    __m256d y1 = _mm256_add_pd( q1, _mm256_mul_pd( x1, h1));
    for(i = 2; i < nb; i++)
      {
        h1 = _mm256_broadcast_sd(&hh[i-(2 -1)]);
        h2 = _mm256_broadcast_sd(&hh[ldh+i-(2 -2)]);
        q1 = _mm256_load_pd(&q[i*ldq]);
        x1 = _mm256_add_pd( x1, _mm256_mul_pd( q1,h1));
        y1 = _mm256_add_pd( y1, _mm256_mul_pd( q1,h2));
      }
    h1 = _mm256_broadcast_sd(&hh[nb-(2 -1)]);
    q1 = _mm256_load_pd(&q[nb*ldq]);
    x1 = _mm256_add_pd( x1, _mm256_mul_pd( q1,h1));
   __m256d tau1 = _mm256_broadcast_sd(hh);
   __m256d tau2 = _mm256_broadcast_sd(&hh[ldh]);
   __m256d vs = _mm256_broadcast_sd(&s);
    h1 = _mm256_xor_pd(tau1, sign);
   x1 = _mm256_mul_pd( x1, h1);
   h1 = _mm256_xor_pd(tau2, sign);
   h2 = _mm256_mul_pd( h1, vs);
   y1 = _mm256_add_pd( _mm256_mul_pd( y1,h1), _mm256_mul_pd( x1,h2));
   q1 = _mm256_load_pd(&q[0]);
   q1 = _mm256_add_pd( q1, y1);
   _mm256_store_pd(&q[0], q1);
   h2 = _mm256_broadcast_sd(&hh[ldh+1]);
   q1 = _mm256_load_pd(&q[ldq]);
   q1 = _mm256_add_pd( q1, _mm256_add_pd( x1, _mm256_mul_pd( y1, h2)));
   _mm256_store_pd(&q[ldq], q1);
   for (i = 2; i < nb; i++)
   {
    h1 = _mm256_broadcast_sd(&hh[i-(2 -1)]);
    h2 = _mm256_broadcast_sd(&hh[ldh+i-(2 -2)]);
     q1 = _mm256_load_pd(&q[i*ldq]);
     q1 = _mm256_add_pd( q1, _mm256_add_pd( _mm256_mul_pd( x1,h1), _mm256_mul_pd( y1, h2)));
     _mm256_store_pd(&q[i*ldq], q1);
   }
   h1 = _mm256_broadcast_sd(&hh[nb-(2 -1)]);
   q1 = _mm256_load_pd(&q[nb*ldq]);
   q1 = _mm256_add_pd( q1, _mm256_mul_pd( x1, h1));
   _mm256_store_pd(&q[nb*ldq], q1);
}
#endif
