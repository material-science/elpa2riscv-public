#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include "tracing.h"

extern int nops;
static void hh_trafo_kernel_12_generic_double( double complex *q, double *hh[2], int nb, int nq, double s );
static void hh_trafo_kernel_8_generic_double( double complex *q, double *hh[2], int nb, int nq, double s );
static void hh_trafo_kernel_4_generic_double( double complex *q, double *hh[2], int nb, int nq, double s );

static void hh_trafo_generic( double *q, double *hh[2], int nb, int nq ) {
  int i;
  initialize_tracing();
  enable_tracing();
  trace_event(1000,1);

  double s = hh[1][1];
  for( int i = 2; i < nb; i++ )
    s = s + hh[1][i]*hh[0][i-1];

  for (i=0; i<nq-8; i+=12)
    hh_trafo_kernel_12_generic_double( (double complex*)&q[i], hh, nb, nq, s );

  if (nq-i>4) hh_trafo_kernel_8_generic_double( (double complex*)&q[i], hh, nb, nq, s );
  else if (nq-i>0) hh_trafo_kernel_4_generic_double( (double complex*)&q[i], hh, nb, nq, s );
  trace_event(1000,0);
}

static void hh_trafo_kernel_12_generic_double( double complex *q, double *hh[2],
            int nb, int nq, double s ) {
  int C_ldq=nq/2;
  double h1, h2, tau1, tau2;

  double complex x1 = q[C_ldq+0];
  double complex x2 = q[C_ldq+1];
  double complex x3 = q[C_ldq+2];
  double complex x4 = q[C_ldq+3];
  double complex x5 = q[C_ldq+4];
  double complex x6 = q[C_ldq+5];

  double complex y1 = q[0] + q[C_ldq+0]*hh[1][1];
  double complex y2 = q[1] + q[C_ldq+1]*hh[1][1];
  double complex y3 = q[2] + q[C_ldq+2]*hh[1][1];
  double complex y4 = q[3] + q[C_ldq+3]*hh[1][1];
  double complex y5 = q[4] + q[C_ldq+4]*hh[1][1];
  double complex y6 = q[5] + q[C_ldq+5]*hh[1][1];

  for (int i=2; i< nb; i++) {
    h1 = hh[0][i-1];
    h2 = hh[1][i];
    x1  = x1 + q[i*C_ldq+0]*h1;
    y1  = y1 + q[i*C_ldq+0]*h2;
    x2  = x2 + q[i*C_ldq+1]*h1;
    y2  = y2 + q[i*C_ldq+1]*h2;
    x3  = x3 + q[i*C_ldq+2]*h1;
    y3  = y3 + q[i*C_ldq+2]*h2;
    x4  = x4 + q[i*C_ldq+3]*h1;
    y4  = y4 + q[i*C_ldq+3]*h2;
    x5  = x5 + q[i*C_ldq+4]*h1;
    y5  = y5 + q[i*C_ldq+4]*h2;
    x6  = x6 + q[i*C_ldq+5]*h1;
    y6  = y6 + q[i*C_ldq+5]*h2;
  }
  x1 = x1 + q[nb*C_ldq+0]*hh[0][nb-1];
  x2 = x2 + q[nb*C_ldq+1]*hh[0][nb-1];
  x3 = x3 + q[nb*C_ldq+2]*hh[0][nb-1];
  x4 = x4 + q[nb*C_ldq+3]*hh[0][nb-1];
  x5 = x5 + q[nb*C_ldq+4]*hh[0][nb-1];
  x6 = x6 + q[nb*C_ldq+5]*hh[0][nb-1];

  tau1 = hh[0][0];
  tau2 = hh[1][0];
  h1 = -tau1;

  x1 = x1*h1;
  x2 = x2*h1;
  x3 = x3*h1;
  x4 = x4*h1;
  x5 = x5*h1;
  x6 = x6*h1;

  h1 = -tau2;
  h2 = -tau2*s;
  y1 = y1*h1 + x1*h2;
  y2 = y2*h1 + x2*h2;
  y3 = y3*h1 + x3*h2;
  y4 = y4*h1 + x4*h2;
  y5 = y5*h1 + x5*h2;
  y6 = y6*h1 + x6*h2;

  q[0] += y1;
  q[1] += y2;
  q[2] += y3;
  q[3] += y4;
  q[4] += y5;
  q[5] += y6;

  q[C_ldq+0] += x1  + y1 *hh[1][1];
  q[C_ldq+1] += x2  + y2 *hh[1][1];
  q[C_ldq+2] += x3  + y3 *hh[1][1];
  q[C_ldq+3] += x4  + y4 *hh[1][1];
  q[C_ldq+4] += x5  + y5 *hh[1][1];
  q[C_ldq+5] += x6  + y6 *hh[1][1];

  for (int i=2; i< nb; i++) {
    h1 = hh[0][i-1];
    h2 = hh[1][i];
    q[i*C_ldq+0] += x1*h1 + y1*h2;
    q[i*C_ldq+1] += x2*h1 + y2*h2;
    q[i*C_ldq+2] += x3*h1 + y3*h2;
    q[i*C_ldq+3] += x4*h1 + y4*h2;
    q[i*C_ldq+4] += x5*h1 + y5*h2;
    q[i*C_ldq+5] += x6*h1 + y6*h2;
  }
  q[nb*C_ldq+0] += x1*hh[0][nb-1];
  q[nb*C_ldq+1] += x2*hh[0][nb-1];
  q[nb*C_ldq+2] += x3*hh[0][nb-1];
  q[nb*C_ldq+3] += x4*hh[0][nb-1];
  q[nb*C_ldq+4] += x5*hh[0][nb-1];
  q[nb*C_ldq+5] += x6*hh[0][nb-1];
}


static void hh_trafo_kernel_8_generic_double( double complex *q, double *hh[2],
            int nb, int nq, double s ) {
  int C_ldq=nq/2;
  double h1, h2, tau1, tau2;

  double complex x1 = q[C_ldq+0];
  double complex x2 = q[C_ldq+1];
  double complex x3 = q[C_ldq+2];
  double complex x4 = q[C_ldq+3];

  double complex y1 = q[0] + q[C_ldq+0]*hh[1][1];
  double complex y2 = q[1] + q[C_ldq+1]*hh[1][1];
  double complex y3 = q[2] + q[C_ldq+2]*hh[1][1];
  double complex y4 = q[3] + q[C_ldq+3]*hh[1][1];

  for (int i=2; i< nb; i++) {
    h1 = hh[0][i-1];
    h2 = hh[1][i];
    x1  = x1 + q[i*C_ldq+0]*h1;
    y1  = y1 + q[i*C_ldq+0]*h2;
    x2  = x2 + q[i*C_ldq+1]*h1;
    y2  = y2 + q[i*C_ldq+1]*h2;
    x3  = x3 + q[i*C_ldq+2]*h1;
    y3  = y3 + q[i*C_ldq+2]*h2;
    x4  = x4 + q[i*C_ldq+3]*h1;
    y4  = y4 + q[i*C_ldq+3]*h2;
  }
  x1 = x1 + q[nb*C_ldq+0]*hh[0][nb-1];
  x2 = x2 + q[nb*C_ldq+1]*hh[0][nb-1];
  x3 = x3 + q[nb*C_ldq+2]*hh[0][nb-1];
  x4 = x4 + q[nb*C_ldq+3]*hh[0][nb-1];

  tau1 = hh[0][0];
  tau2 = hh[1][0];
  h1 = -tau1;

  x1 = x1*h1;
  x2 = x2*h1;
  x3 = x3*h1;
  x4 = x4*h1;

  h1 = -tau2;
  h2 = -tau2*s;
  y1 = y1*h1 + x1*h2;
  y2 = y2*h1 + x2*h2;
  y3 = y3*h1 + x3*h2;
  y4 = y4*h1 + x4*h2;

  q[0] += y1;
  q[1] += y2;
  q[2] += y3;
  q[3] += y4;

  q[C_ldq+0] += x1  + y1 *hh[1][1];
  q[C_ldq+1] += x2  + y2 *hh[1][1];
  q[C_ldq+2] += x3  + y3 *hh[1][1];
  q[C_ldq+3] += x4  + y4 *hh[1][1];

  for (int i=2; i< nb; i++) {
    h1 = hh[0][i-1];
    h2 = hh[1][i];
    q[i*C_ldq+0] += x1*h1 + y1*h2;
    q[i*C_ldq+1] += x2*h1 + y2*h2;
    q[i*C_ldq+2] += x3*h1 + y3*h2;
    q[i*C_ldq+3] += x4*h1 + y4*h2;
  }
  q[nb*C_ldq+0] += x1*hh[0][nb-1];
  q[nb*C_ldq+1] += x2*hh[0][nb-1];
  q[nb*C_ldq+2] += x3*hh[0][nb-1];
  q[nb*C_ldq+3] += x4*hh[0][nb-1];
}


static void hh_trafo_kernel_4_generic_double( double complex *q, double *hh[2],
            int nb, int nq, double s ) {
  int C_ldq=nq/2;
  double h1, h2, tau1, tau2;

  double complex x1 = q[C_ldq+0];
  double complex x2 = q[C_ldq+1];

  double complex y1 = q[0] + q[C_ldq+0]*hh[1][1];
  double complex y2 = q[1] + q[C_ldq+1]*hh[1][1];

  for (int i=2; i< nb; i++) {
    h1 = hh[0][i-1];
    h2 = hh[1][i];
    x1  = x1 + q[i*C_ldq+0]*h1;
    y1  = y1 + q[i*C_ldq+0]*h2;
    x2  = x2 + q[i*C_ldq+1]*h1;
    y2  = y2 + q[i*C_ldq+1]*h2;
  }
  x1 = x1 + q[nb*C_ldq+0]*hh[0][nb-1];
  x2 = x2 + q[nb*C_ldq+1]*hh[0][nb-1];

  tau1 = hh[0][0];
  tau2 = hh[1][0];
  h1 = -tau1;

  x1 = x1*h1;
  x2 = x2*h1;

  h1 = -tau2;
  h2 = -tau2*s;
  y1 = y1*h1 + x1*h2;
  y2 = y2*h1 + x2*h2;

  q[0] += y1;
  q[1] += y2;

  q[C_ldq+0] += x1  + y1 *hh[1][1];
  q[C_ldq+1] += x2  + y2 *hh[1][1];

  for (int i=2; i< nb; i++) {
    h1 = hh[0][i-1];
    h2 = hh[1][i];
    q[i*C_ldq+0] += x1*h1 + y1*h2;
    q[i*C_ldq+1] += x2*h1 + y2*h2;
  }
  q[nb*C_ldq+0] += x1*hh[0][nb-1];
  q[nb*C_ldq+1] += x2*hh[0][nb-1];
}








#if 0

  subroutine hh_trafo_kernel_8_generic_double(q, hh, nb, ldq, ldh, s)
    implicit none
    integer*4, intent(in)     :: nb, ldq, ldh
    complex*16, intent(inout)  :: q(ldq/2,*)
    real*8, intent(in)     :: hh(ldh,*)
    real*8, intent(in)     :: s
    complex*16     :: x1, x2, x3, x4, y1, y2, y3, y4
    real*8                    :: h1, h2, tau1, tau2
    integer*4                 :: i

    x1 = q(1,2)
    x2 = q(2,2)
    x3 = q(3,2)
    x4 = q(4,2)

    y1 = q(1,1) + q(1,2)*hh(2,2)
    y2 = q(2,1) + q(2,2)*hh(2,2)
    y3 = q(3,1) + q(3,2)*hh(2,2)
    y4 = q(4,1) + q(4,2)*hh(2,2)


    do i=3,nb
       h1 = hh(i-1,1)
       h2 = hh(i,2)
       x1 = x1 + q(1,i)*h1
       y1 = y1 + q(1,i)*h2
       x2 = x2 + q(2,i)*h1
       y2 = y2 + q(2,i)*h2
       x3 = x3 + q(3,i)*h1
       y3 = y3 + q(3,i)*h2
       x4 = x4 + q(4,i)*h1
       y4 = y4 + q(4,i)*h2
    enddo

    x1 = x1 + q(1,nb+1)*hh(nb,1)
    x2 = x2 + q(2,nb+1)*hh(nb,1)
    x3 = x3 + q(3,nb+1)*hh(nb,1)
    x4 = x4 + q(4,nb+1)*hh(nb,1)

    tau1 = hh(1,1)
    tau2 = hh(1,2)

    h1 = -tau1
    x1 = x1*h1
    x2 = x2*h1
    x3 = x3*h1
    x4 = x4*h1
    h1 = -tau2
    h2 = -tau2*s
    y1 = y1*h1 + x1*h2
    y2 = y2*h1 + x2*h2
    y3 = y3*h1 + x3*h2
    y4 = y4*h1 + x4*h2
    q(1,1) = q(1,1) + y1
    q(2,1) = q(2,1) + y2
    q(3,1) = q(3,1) + y3
    q(4,1) = q(4,1) + y4
    q(1,2) = q(1,2) + x1 + y1*hh(2,2)
    q(2,2) = q(2,2) + x2 + y2*hh(2,2)
    q(3,2) = q(3,2) + x3 + y3*hh(2,2)
    q(4,2) = q(4,2) + x4 + y4*hh(2,2)


    do i=3,nb
       h1 = hh(i-1,1)
       h2 = hh(i,2)
       q(1,i) = q(1,i) + x1*h1 + y1*h2
       q(2,i) = q(2,i) + x2*h1 + y2*h2
       q(3,i) = q(3,i) + x3*h1 + y3*h2
       q(4,i) = q(4,i) + x4*h1 + y4*h2
    enddo

    q(1,nb+1) = q(1,nb+1) + x1*hh(nb,1)
    q(2,nb+1) = q(2,nb+1) + x2*hh(nb,1)
    q(3,nb+1) = q(3,nb+1) + x3*hh(nb,1)
    q(4,nb+1) = q(4,nb+1) + x4*hh(nb,1)


  end subroutine
  ! --------------------------------------------------------------------------------------------------

  subroutine hh_trafo_kernel_4_generic_double(q, hh, nb, ldq, ldh, s)
    implicit none
    integer*4, intent(in)    :: nb, ldq, ldh
    complex*16, intent(inout) :: q(ldq/2,*)
    real*8, intent(in)    :: hh(ldh,*)
    real*8, intent(in)    :: s
    complex*16    :: x1, x2, y1, y2
    real*8                :: h1, h2, tau1, tau2
    integer*4                :: i

    x1 = q(1,2)
    x2 = q(2,2)

    y1 = q(1,1) + q(1,2)*hh(2,2)
    y2 = q(2,1) + q(2,2)*hh(2,2)


    do i=3,nb
       h1 = hh(i-1,1)
       h2 = hh(i,2)
       x1 = x1 + q(1,i)*h1
       y1 = y1 + q(1,i)*h2
       x2 = x2 + q(2,i)*h1
       y2 = y2 + q(2,i)*h2
    enddo

    x1 = x1 + q(1,nb+1)*hh(nb,1)
    x2 = x2 + q(2,nb+1)*hh(nb,1)

    tau1 = hh(1,1)
    tau2 = hh(1,2)

    h1 = -tau1
    x1 = x1*h1
    x2 = x2*h1
    h1 = -tau2
    h2 = -tau2*s
    y1 = y1*h1 + x1*h2
    y2 = y2*h1 + x2*h2

    q(1,1) = q(1,1) + y1
    q(2,1) = q(2,1) + y2
    q(1,2) = q(1,2) + x1 + y1*hh(2,2)
    q(2,2) = q(2,2) + x2 + y2*hh(2,2)

    do i=3,nb
       h1 = hh(i-1,1)
       h2 = hh(i,2)
       q(1,i) = q(1,i) + x1*h1 + y1*h2
       q(2,i) = q(2,i) + x2*h1 + y2*h2
    enddo

    q(1,nb+1) = q(1,nb+1) + x1*hh(nb,1)
    q(2,nb+1) = q(2,nb+1) + x2*hh(nb,1)

  end subroutine

#endif


void real_generic( double *bcast_buffer, double* q, int ncols, int nbw, int nq, int ldq ) {
  int err;
  double *w[2];

  nops=0;
  int off=0;
  for (int i=0; i< nq; i+=ldq) {
    int lnq = ldq< nq-i ? ldq : nq-i;
    for (int j=ncols-1; j>=0; j-=2) {
      w[0] = &bcast_buffer[j*nbw];
      w[1] = &bcast_buffer[(j-1)*nbw];
      hh_trafo_generic( &q[off+(j-1)*lnq], w, nbw, lnq );
    }
    off += lnq*(ncols+nbw);
  }
  nops = (nq*nbw*8-2*nq+4)*ncols/2 + 2*(nbw-2);
}
