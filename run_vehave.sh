#!/bin/bash
mkdir -p vehave_prv_traces
ID=0
i=2; while [ ${i} -le $# ]; do
  [ "${!i}" -eq "${!i}" ] &> /dev/null && ID=${!i}
  i=$((i+1))
done
name=$( basename $1 )-${ID}
#VEHAVE_TRACE_SINGLE_THREAD=1 VEHAVE_VECTOR_LENGTH=$((256*64)) VEHAVE_TRACE_FILE=vehave_prv_traces/$name.trace VEHAVE_DEBUG_LEVEL=0 vehave ./$@
  VEHAVE_TRACE_SINGLE_THREAD=1 VEHAVE_TRACE=1 \
  VEHAVE_VECTOR_LENGTH=$((256*64)) VEHAVE_TRACE_FILE=vehave_prv_traces/$name.trace \
  VEHAVE_DEBUG_LEVEL=0 vehave ./$@
vehave2prv --output-dir vehave_prv_traces vehave_prv_traces/$name.trace 
